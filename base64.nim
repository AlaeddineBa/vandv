#
#
#            Nim's Runtime Library
#        (c) Copyright 2010 Andreas Rumpf
#
#    See the file "copying.txt", included in this
#    distribution, for details about the copyright.
#

## This module implements a base64 encoder and decoder.
##
## Encoding data
## -------------
##
## In order to encode some text simply call the ``encode`` procedure:
##
##   .. code-block::nim
##      import base64
##      let encoded = encode("Hello World")
##      echo(encoded) # SGVsbG8gV29ybGQ=
##
## Apart from strings you can also encode lists of integers or characters:
##
##   .. code-block::nim
##      import base64
##      let encodedInts = encode([1,2,3])
##      echo(encodedInts) # AQID
##      let encodedChars = encode(['h','e','y'])
##      echo(encodedChars) # aGV5
##
## The ``encode`` procedure takes an ``openarray`` so both arrays and sequences
## can be passed as parameters.
##
## Decoding data
## -------------
##
## To decode a base64 encoded data string simply call the ``decode``
## procedure:
##
##   .. code-block::nim
##      import base64
##      echo(decode("SGVsbG8gV29ybGQ=")) # Hello World

const
  cb64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"

template encodeInternal(s: expr, lineLen: int, newLine: string): stmt {.immediate.} =
  ## encodes `s` into base64 representation. After `lineLen` characters, a
  ## `newline` is added.
  var total = ((len(s) + 2) div 3) * 4
  var numLines = (total + lineLen - 1) div lineLen
  if numLines > 0: inc(total, (numLines-1) * newLine.len)

  result = newString(total)
  var i = 0
  var r = 0
  var currLine = 0
  while i < s.len - 2:
    var a = ord(s[i])
    var b = ord(s[i+1])
    var c = ord(s[i+2])
    result[r] = cb64[a shr 2]
    result[r+1] = cb64[((a and 3) shl 4) or ((b and 0xF0) shr 4)]
    result[r+2] = cb64[((b and 0x0F) shl 2) or ((c and 0xC0) shr 6)]
    result[r+3] = cb64[c and 0x3F]
    inc(r, 4)
    inc(i, 3)
    inc(currLine, 4)
    if currLine >= lineLen and i != s.len-2:
      for x in items(newLine):
        result[r] = x
        inc(r)
      currLine = 0

  if i < s.len-1:
    var a = ord(s[i])
    var b = ord(s[i+1])
    result[r] = cb64[a shr 2]
    result[r+1] = cb64[((a and 3) shl 4) or ((b and 0xF0) shr 4)]
    result[r+2] = cb64[((b and 0x0F) shl 2)]
    result[r+3] = '='
    if r+4 != result.len:
      setLen(result, r+4)
  elif i < s.len:
    var a = ord(s[i])
    result[r] = cb64[a shr 2]
    result[r+1] = cb64[(a and 3) shl 4]
    result[r+2] = '='
    result[r+3] = '='
    if r+4 != result.len:
      setLen(result, r+4)
  else:
    if r != result.len:
      setLen(result, r)
    #assert(r == result.len)
    discard

proc encode*[T:SomeInteger|char](s: openarray[T], lineLen = 75, newLine="\13\10"): string =
  ## encodes `s` into base64 representation. After `lineLen` characters, a
  ## `newline` is added.
  ##
  ## This procedure encodes an openarray (array or sequence) of either integers
  ## or characters.
  encodeInternal(s, lineLen, newLine)

proc encode*(s: string, lineLen = 75, newLine="\13\10"): string =
  ## encodes `s` into base64 representation. After `lineLen` characters, a
  ## `newline` is added.
  ##
  ## This procedure encodes a string.
  encodeInternal(s, lineLen, newLine)

proc decodeByte(b: char): int {.inline.} =
  case b
  of '+': result = ord('>')
  of '0'..'9': result = ord(b) + 4
  of 'A'..'Z': result = ord(b) - ord('A')
  of 'a'..'z': result = ord(b) - 71
  else: result = 63

proc decode*(s: string): string =
  ## decodes a string in base64 representation back into its original form.
  ## Whitespace is skipped.
  const Whitespace = {' ', '\t', '\v', '\r', '\l', '\f'}
  var total = ((len(s) + 3) div 4) * 3
  # total is an upper bound, as we will skip arbitrary whitespace:
  result = newString(total)

  var i = 0
  var r = 0
  while true:
    while s[i] in Whitespace: inc(i)
    if i < s.len-3:
      var a = s[i].decodeByte
      var b = s[i+1].decodeByte
      var c = s[i+2].decodeByte
      var d = s[i+3].decodeByte

      result[r] = chr((a shl 2) and 0xff or ((b shr 4) and 0x03))
      result[r+1] = chr((b shl 4) and 0xff or ((c shr 2) and 0x0F))
      result[r+2] = chr((c shl 6) and 0xff or (d and 0x3F))
      inc(r, 3)
      inc(i, 4)
    else: break
  assert i == s.len
  # adjust the length:
  if i > 0 and s[i-1] == '=':
    dec(r)
    if i > 1 and s[i-2] == '=': dec(r)
  setLen(result, r)

when isMainModule:
  assert encode("leasure.") == "bGVhc3VyZS4="
  assert encode("easure.") == "ZWFzdXJlLg=="
  assert encode("asure.") == "YXN1cmUu"
  assert encode("sure.") == "c3VyZS4="

  const longText = """
 Number 47, Winter 2004 cartographic perspectives 
cartographic perspectives
Number 47, Winter 2004
journal of the North American Cartographic Information Society
NACIS WEB SITE
www.nacis.org
in this issue
OPINION COLUMN
Cartography 2003 4
Judy M. Olson
featured articleS
Mapping September 11, 2001: Cartographic Narrative 13
in the Print Media
Robert R. Churchill and Suzanne J. Slarsky
Hal Shelton Revisted: Designing and Producing 28
Natural-Color Maps with Satellite Land Cover Data
Tom Patterson and Nathaniel Vaughn Kelso
CARTOGRAPHIC TECHNIQUES
Small Type, Screens and Color in a Postscript Offset 56
Printing Environment
Nat Case
BOOK REVIEWS
The Man Who Flattened the Earth: Maupertuis and the 60
Sciences in the Enlightenment
Reviewed by Judith A. Tyner
Cataloging Sheet Maps, the Basics 61
Reviewed by Christopher H. Mixon
COLOR FIGURES
Cartography 2003 65
Mapping September 11, 2001: Cartographic Narrative 67
in the Print Media
(continued on page 3)
Letter from the Editor
Dear Members of NACIS,
I am happy to report that with the
publication of this issue, CP47,
Cartographic Perspectives is back on
publication schedule! That is probably
news big enough to end this
column on…but I will forge ahead
never the less.
To ensure new ideas and a
vibrant, thriving journal, I have
assembled a very diverse editorial
board for 2004 through 2006. I
would like to dedicate some space
in this column to introduce to you
both new and returning members
of CP’s editorial board. First off,
CP welcomes the following five
members: Michael Leitner from the
Department of Geography and
Anthropology at Louisiana State
University; Nadine Schuurman from
the Department of Geography
at Simon Fraser University; Erik
Steiner from the InfoGraphics Lab
at the University of Oregon; Dan
Van Dorn from Map Link in Santa
Barbara; Denis Wood who is an
(continued on page 3)
 cartographic perspectives Number 47, Winter 2004
Assistant Editor
James R. Anderson, Jr.
FREAC
Florida State University
Tallahassee, FL 32306-2641
(850) 644-2883
fax: (850) 644-7360
janderson@admin.fsu.edu
Book Review Editor
Ren Vasiliev
Department of Geography
SUNY @ Geneseo
1 College Circle
Geneseo, NY 14454
(585) 245-5297
vasiliev@geneseo.edu
Cartographic Techniques Editor
Charlie Frye
ESRI
380 New York Street
Redlands, CA 92373
(909) 793-2853
cfrye@esri.com
Opinion Column Editor
Scott Freundschuh
Department of Geography
University of Minnesota, Duluth
329 Cina Hall
Duluth, MN 55812
(218) 726-6226
sfreunds@d.umn.edu
Map Library Bulletin Board Editor
Chris Mixon
Auburn University Libraries
231 Mell Street
Auburn University
Auburn, AL 36849-5606
(334) 844-1738
mixonch@auburn.edu
Editor
Scott M. Freundschuh
Department of Geography
University of Minnesota, Duluth
329 Cina Hall
Duluth, MN 55812
(218) 726-6226
fax: (218) 726-6386
sfreunds@d.umn.edu
journal of the
North American Cartographic Information Society
cartographic perspectives
ISSN 1048-9085
Cartographic Perspectives is published triannually
© 2004 North American Cartographic Information Society
Elisabeth S. Nelson
Univ. of N. Carolina - Greensboro
Margaret Pearce
University of Guelph
Nadine Schuurman
Simon Fraser University
Erik Steiner
University of Oregon
Dan Van Dorn
Map Link
Ren Vasiliev
State Univ. of New York at Geneseo
Denis Wood
Independent Scholar
Sara Fabrikant
Univ. of California - Santa Barbara
Ken Foote
University of Colorado
Pat Gilmartin
University of South Carolina
John B. Krygier
Ohio Wesleyan University
Michael Leitner
Louisiana State University
Robert Lloyd
University of South Carolina
Jan Mersey
University of Guelph
Cartographic Perspectives
EDITORIAL BOARD
about the cover
The cover image was created by Matt
Knutzen, artist, cartographer and Assistant
Chief Librarian of the Map Division
of the New York Public Library.
 Number 47, Winter 2004 cartographic perspectives 
Independent Scholar in Raleigh,
NC. CP also welcomes back the
following nine people, eight who
are returning for a second term on
CP’s editorial board: Sara Fabrikant
from the Department of Geography
at University of California in
Santa Barbara; Ken Foote from the
Department of Geography at the
University of Colorado at Boulder;
Pat Gilmartin from the Department
of Geography at the University
of South Carolina; John B. Krygier
from the Department of Geology
and Geography at Ohio Wesleyan;
Robert Lloyd from the Department
of Geography at the University of
South Carolina; Jan Mersey from
the Department of Geography at
the University of Guelph; Elisabeth
S. Nelson from the Department
of Geography at the University
of North Carolina in Greensboro;
Margaret Pearce from the Department
of Geography at Western
Michigan University; Ren Vasiliev
(serving a 3rd term) from the Department
of Geography at the State
University of New York, College at
Geneseo. I very much look forward
to working with is talented
group of carto-geographers.
This issue of CP also marks a
change in three section editors, as
well as changes in two sections of
CP. First off, CP welcomes Chris
Mixon from Auburn University
Libraries as the new Map Library
Bulletin Board Editor. Chris has
some great ideas for this section, so
expect to see some new directions
in this column. The Essay Section
of CP is transmogrifying into the
Opinion Column, which will bein
this issue (continued)
Hal Shelton Revisted: Designing and Producing 69
Natural-Color Maps with Satellite Land Cover Data
Small Type, Screens and Color in a Postscript Offset 81
Printing Environment
come an occasional piece in CP.
Matt McGranaghan is stepping
down as editor of this column
and is moving on to more surf
and sun in his life. Thank you
Matt for your time and energies!
Lastly, Jeremy Crampton has
decided to step down as section
editor for the On-Line Mapping
column. After much discussion
in Jacksonville, it was decided
that this section would no longer
be a separate theme in CP, but
instead would be integrated into
the other parts of CP. I want to
extend special thanks to Jeremy
for his foresight in bringing online
mapping issues center stage,
and for facilitating some great
debates and research in this area.
As outlined in the last issue of
CP, and now in this issue, Cartographic
Perspectives has certainly
experienced many changes in
the past couple of years. These
changes, I believe, make CP more
than just a solid contribution to
the cartographic literature. These
changes are enhancing a unique
publication that makes it possible
for all members of our diverse
cartographic community to have
a voice… to put our thoughts and
ideas out there for debate…to illustrate
our diverse cartographies
that coalesce to form this community
we call the North American
Cartographic Information
Society…in color, if we choose.
So what’s up for this year?
This issue of CP is born in part
from papers that were presented
at the 2003 NACIS meeting and
during Practical Cartography
Day in Jacksonville, FL. The lead
off paper in this issue is Judy
Olson’s plenary titled Cartography
2003. Judy summarizes for us
where she thinks cartography has
been, where is it now, and some of
the things we are likely to see in
the future. Next is a paper by Tom
Patterson and Nathaniel Kelso
titled Hal Shelton Revisited: Designing
and Producing Natural-Color
Maps with Satellite Land Cover Data.
This paper was part of the Practical
Cartography Day, and provides
a wonderful account of the work
of Hal Sheldon, and its impact on
visualizing satellite data. Last is
a paper by Bob Churchill titled
Mapping September 11, 2001: Cartographic
Narrative in the Print Media.
This paper shows how maps were
used in the media to report on
the terrorist attacks of 9-11. The
Techniques Column in this issue
has a piece by Nate Case that was
also presented as part of Practical
Cartography Day. This paper
provides many tips for successful
duplication of small type, screens
and color in a PostScript offset
printing environment.
The next issue of CP, #48, is a
theme issue that will feature three
papers on maps made by First
Nations Peoples (i.e., indigenous
cartographies) as well as a preface
by G. Malcolm Lewis. The final issue
for 2004, CP49 is taking shape
at this time and will have papers
on metadata for maps, and on representing
confidential information
on maps.
As always, I welcome any
thoughts, ideas, or compliments
you may have for Cartographic
Perspectives.
Warmest Regards,
Scott Freundschuh, Editor
  cartographic perspectives Number 47, Winter 2004
Cartography 2003
Judy M. Olson
Department of Geography
Michigan State University
East Lansing, MI 48824
olsonj@msu.edu
Note from the Editor: This material was first presented at the NACIS banquet
Plenary, October 10, 2003, in Jacksonville, FL.
artography 2003 is a rich and awesome topic. I am presenting here a
sort of potpourri of topics and images that characterize the field in
2003. Not everything that is referred to is strictly from this year, but
then, not everything we are or see today was invented or even updated in
2003!
My comments are in five parts: enduring content (that is, the stuff of the
discipline that “stays with us” and is not new to the field in 2003), the definition
of cartography (some general observations about how it has shifted
over the years rather than any attempt at an airtight definition), maps today
(which does distinguish contemporary cartography from earlier years),
ICC 2003 (a few comments about the International Cartographic Conference
2003 because it was indeed an event of this year and a significant
one), and finally a few words about where from here (one modest observation
in the grand scheme of where things might go from here that may be
worth sharing).
Everyone in cartography knows that it is a constantly changing discipline.
I remember a white-haired, but definitely not old, British colleague,
Harold Fullard, once commenting that he had lived from the Stone Age
to the computer age. What he meant, of course, was that he started out in
cartography when limestone lithographic printing plates were still around
and he was still in the business as computers were becoming the tools for
making and even displaying maps. People in my general age group have
lived from SYMAP, that first user-oriented mapping program, to palm
tops, map-bearing cell phones, and ubiquitous mapmaking -- or at least
more or less ubiquitous map access.
In its ever-changing condition, some components of cartography simply
go away. Color separation techniques such as scribing and peelcoats
are gone, as is the construction of projections from tables and formulas, a
cartographic activity that is far more likely to elicit groans than nostalgic
comments from those who remember. But those are technical things; we
expect that principles are more enduring, and they are. The principles of
matching symbol dimensions with data characteristics, choosing mapping
methods, and manipulating data in sound ways for display are
still largely associated with the term cartography. Certainly presentation
mapping is in the bailiwick of the discipline, as is map design, at least if it
is considered explicitly. Map projections, even since the post renaissance
splitting of disciplines, have never been exclusively cartographic territory,
but choosing them has been with us for many decades. Map appreciation,
by which we are generally referring to knowledge about maps as opposed
to skill in making maps, is also a part of cartography.
This list of components is not exhaustive, but I mention these things because
the definition of the field has changed and there are also things cartography
no longer owns. It might be helpful to consider the very simple
definition of cartography that I generally use in talking to my lower-level
class about what it means. I say that, in simplest terms, cartography is “the
body of knowledge about maps.” This definition is short and it expresses
cartography as an intellectual discipline rather than a skill or technique or
“Cartography 2003 is a rich
and awesome topic.”
“. . . cartography . . . is . . .
constantly changing . . .”
“. . . the definition of the field
has changed . . .”
cartographic perspectives Number 47, Winter 2004 
the activity of mapmaking. There are indeed skills and techniques that are
part of cartography and we do make maps, but it is the head full of knowledge
that skilled people bring to the execution of maps that makes them
good at what they do. And there are theoreticians of cartography as well
as practitioners, and intellectually they have a lot in common. It used to
be, however, that just about anything to do with maps was called cartography.
That has changed in recent decades. We now share a lot of territory
with GIS and (geo)visualization.
This sharing of territory brings up the question of whether cartography
is dead. Mapping certainly is not, and a rose by any other name is still a
rose. There is, in fact, a lot of exciting mapping going on these days, and
there is a lot of knowledge about maps as well, spread over more people,
not fewer. Michael Goodchild has stated that cartography (as a discipline)
is being marginalized (Goodchild, 2000). We can hardly deny that. In higher
education there are fewer and fewer courses labeled cartography, and at
my institution, at least, when we put such a label on the class, few sign up.
We also find evidence when we look at the jobs being advertised in geography.
A few years ago, I tracked the advertisements for positions in Jobs
in Geography, published by the Association of American Geographers
(AAG, various dates). The results are shown in Figures 1 and 2. These are
primarily academic jobs, not all jobs associated with cartography, but they
reflect what is going on in the incubators for cartographers.
The years covered were from 1981-82 through 1996-97. Figure 1 shows
all new postings (top line) and the ones referring to cartography, GIS, and
remote sensing. The total for all three of those areas is the highest of four
lower lines, but it is not the addition of the separate numbers because
many ads mentioned more than one of these areas. They are counted for
each of the individual categories mentioned (the three lowest lines) but
only once for the line representing all of three of them. The three areas
accounted for a substantial portion of all ads, especially in the ’90s. Individually,
cartography peaked in ‘84-85, declined, and leveled out; GIS
rose from first entries to the dominant of the three; and remote sensing
remained relatively flat.
Figure 2 shows the proportion of all jobs mentioning any of these areas,
and the shifting dominance is even more pronounced. Cartography declines,
GIS rises, remote sensing fluctuates and levels off.
Figure 1. Jobs in Geography (JIG) job listings, 1981-82 through 1996-97. The top line indicates
the total number of new postings, the next one the number of ads mentioning cartography, GIS,
and/or remote sensing. The remaining three lines indicate the numbers for each of cartography,
GIS, and remote sensing; an ad mentioning more than one is counted in more than one line.
Source: AAG, various dates. Acknowledgement: Amy Lobben assisted in the compilation.
Figure 2. Relative shares. The values in Figure 1 are
converted to proportion of all cartography, GIS, and
remote sensing listings. Values do not add to 100%
because some ads list more than one of these areas.
“We now share a lot of
territory with GIS and
(geo)visualization.”
“. . . cartography [jobs] peaked
in ‘84-85 . . .”
  cartographic perspectives Number 47, Winter 2004
I was going to update this set of data to take the graphs through 2003,
but that is not a straightforward thing to do. I recall that some of the ads
in the time period of the graphs mentioned terms that were not explicitly
geographic information systems, cartography, or remote sensing, and I
may even have forgotten how complicated the vocabulary was. But here is
some of the phrasing in 2002-2003:
Geographic Information Science…with a focus on visualization or
health applications. (AAG, 38:7, p. 27)
Digital Technology and Communication (35:7, p.22)
Spatial Data Laboratory Supervisor/Instructor (38:7, p. 27)
Society and Information Technology (35:7, p. 22)
Assistant Professor GIS/Remote Sensing, PhD in forestry or related
discipline. (38:7, p. 27)
Postgraduate Researcher..(diseases; fieldwork)…. A Bachelor’s Degree
in economics…preferred…knowledge of GIS and image analysis and
spatio-temporal model(ing)…. (38:6, p. 18)
The lexicon, it seems, has changed in recent years. Yes, cartography
does still appear in some of the job ads, as does GIS and certainly remote
sensing, but many of the traditional terms have been replaced with new
ones, reflecting new ways of looking at how geography is subdivided,
and, more accurately, at the way departments are looking at the packages
of knowledge and skills wanted in individual faculty members. The
old terms (including GIS) do not have the cache they did a few years
ago.
Whether all this is good or bad is open to interpretation. The relative
number of new PhDs and new faculty members who identify explicitly
with cartography dropped drastically enough in the ’90s that it
showed up as an age gap—a degree age gap that is, the length of time
since someone completed their terminal degree. When the U.S. National
Committee for ICA (International Cartographic Association; see ICA,
1999 and USNC, 2003) solicited applicants for travel funding to the
International Cartographic Conference-Beijing 2001, the old guard, that
is, those with older PhDs, were interested in going to the Conference,
as were a cadre of young graduate students and assistant professors
who might think of themselves as associated with GIScience, visualization,
or cartography. But the mid-career applicants were largely missing—noticeably
enough that for the next ICC (Durban, August, 2003),
NSF granted funds not only for young scholars (as they have in the past)
but to support a few mid-career cartographic scholars as well. Unfortunately,
we could not get enough of them to Durban to use up the funds
so allocated. Finding young scholars was no problem. Cartography
might have become a marginalized term in recent years, but people are
recognizing the common mapping interests among a variety of labels,
and we are becoming comfortable with defining ourselves with multiple
labels. Cartographer, GIScience person, visualization scientist, planner
with interests in mapping and spatial data—we probably all have some
combination of such labels at this point.
Though the changing definition is important, and the jobs data are
interesting, there are many more things going on in cartography and
they are of far broader interest and visibility. Here are some examples of
trends that characterize what is going on in the field at the moment, in
other words, that characterize what has grown out of the recent history
of the discipline. (1) Cartographic visualization and the use of maps for
discovery has definitely come into its own and is inspiring a considerable
“The lexicon . . . has changed in
recent years.”
“. . . we probably all have some
combination of . . . labels at this
point.”
cartographic perspectives Number 47, Winter 2004
amount of the research at this point. Research on presentation maps, on
the other hand, is far less prominent than it once was. (2) Cartographic
critique, the application of critical theory within the discipline, is a prominent
part of our literature and thinking. Brian Harley’s (1989) seminal
article is now but one of many examples as cartographers and critics have
followed his lead. (3) “Publication” is now as likely to mean that something
is being put onto the Web or onto some other digital medium, as it
is to mean that it is being put into print. (4) Mapmakers have shifted to
“almost anyone”, although some of the “mapmaking” is a lot more like
ordering a map than making one. (5) Agencies traditionally associated
with the production of maps are now far more involved in producing and
delivering data. The National Map might be considered a case in point for
delivering data but actually takes us a step further. It is the digital replacement
of our very aging topographic map series in the U.S. and is not just
a large database coming from an agency, but is envisioned as “public domain
core geographic data…that other agencies can extend, enhance, and
reference as they concentrate on maintaining other data that are unique to
their needs.” (USGS, 2003-11-06). In other words, cartographic conceptualization
is serving up infrastructure and is not simply delivering a set of
data, much less producing a map in the traditional sense.
As we are making these transitions in our cartographic ways, we can
point to many products and practices that give us more concrete images of
cartography in 2003. Again I am being selective, but notice how these are
knitted into the broad social context, and are not internal matters in our
profession.
Anyone with an Internet connection can now get detailed location
maps, and such products as the ones generated on <www.mapquest.com>
are good examples of how people “order” maps instead of make them.
One can change scale and location and a few other things but one does not
need to make a whole lot of cartographic decisions. And when we have
our (Mapquest™) map up on the screen, we can click on the air photo tab
and see what the area looks like from that perspective.
In 2003, we take for granted the ability to find public domain reference
maps on the web. A good example of a site with such maps is as The World
Factbook (CIA, 2003). Figure 3 shows the CIA’s map of Azerbaijan. With a
reasonable printer we can produce our own paper copy and it will generally
exceed the quality of anything we produced on the copy machine
using the library’s copy.
We can also use Google™ or other search engines to find most any
place, including, say, a remote game reserve in Africa, and find just where
it is. Last summer, for example, I found a map of South Africa showing
Phinda, a private game reserve in South Africa, to share with member of
my family curious as to where I was going on my way to the International
Cartographic Conference in Durban.
In 2003 we can probably find out more about some stranger’s property
than most of us would think appropriate. Cabarrus County (2001), in
North Carolina, for example, has a website that shows property configuration
(Figure 4), and by clicking on the property we can find out who owns
it, when they bought it, how much they paid, and various other pieces
of information. On some property map websites, we can even pull up a
map of the resident’s polling place if we want to know where they vote.
Fortunately, law protects their ballots! On some of these sites, the property
maps come with background air photos. All of this information has
long been “public” in the U.S., but why everyone in the world with an
Internet connection should have such convenient and instant access to it
is an appropriate question to ask. Cartography and related disciplines in
Figure 3. A public domain reference map of
Azerbaijan. Source: CIA Factbook, 2003. (see
page 65 for color version)
Figure 4. A property map on the web. Source:
Cabarrus County, 2001. (see page 65 for color
version)
  cartographic perspectives Number 47, Winter 2004
2003 have some serious issues related to privacy and the balance between
individual and public good (Curry, 1999).
As an example of the beauty and flexibility of modern mapping, “A
Tapestry of Time and Terrain” (Vigil et al., 2003) serves very well, even
if it has been on the Internet for a few years now. A combination of the
King and Beikman (1974) geologic map of the US and the Thelin and Pike
(1991) landforms map, it is a stunning visual piece (Figure 5). Anyone
with enough memory chips, a high quality printer, and a connection to
the Internet can have a copy of it. Having helped a local theatre recently to
decorate its stage and lobby for a production of Lonely Planet (Dietz, 1994),
the play about AIDS that is set in a map store, I was especially interested
to watch people during the intermission reacting to the patterns on this
artful but reekingly-scientific map, which we had posted in the lobby.
After a dearth of thematic maps with the 1990 Census, we can now go
onto the web and find Mapping Census 2000 (Brewer and Suchan, 2001)
(Figure 6), and we can print out high-quality copies on our local color
printer. As an instructor in classes, it is refreshing to be able to print out
multiple copies of high-quality maps of census data for students to use
in class exercises and exams. The way in which we approach the teaching
of map-related courses is, in fact, worlds away from a decade ago, in
part because of the maps we can access, not just because we have different
facilities for making maps.
And speaking of teaching, there are downloadable tools and materials
on the web such as those from John Carnes’s site on utilizing map grids
(Carnes, 2002). His grid for zeroing in on UTM locations on topographic
maps is a very useful tool for getting across to students the fundamental
reason for using rectangular coordinate systems—convenience and speed.
This little grid fits any one-km grid cell printed on any 1:24,000 topographic
map in the US that is on the UTM projection (Figure 7), and the
search-and-rescue mission example that Carnes uses to demonstrate the
point is a captivating application and readily available.
We also have tools on the Internet for making maps rather than reading
them. That sticky problem of selecting colors for maps is eased with
ColorBrewer, with which one can select a reasonable set of colors with
a few mouse clicks (Brewer, 2002). Select, say, 5 classes and her colorful
red-yellow-blue scheme as illustrated in Figure 8, and the user can see
it on the prototype map, learn that it should be fine for folks with color
Figure 6. A map from Mapping Census 2000.
Source: Brewer and Suchan, 2001. (see page
65 for color version)
Figure 7. The location grid and example excerpt from a topographic
map. The grid can be printed on transparent medium and placed
over the map. Source: Carnes, 2002. Used with permission.
Figure 5. Excerpts from “A Tapestry of Time
and Terrain.” Image has been rearranged for
this illustration. Source: Vigil et al., 2003. (see
page 65 for color version)
cartographic perspectives Number 47, Winter 2004 
impaired vision (the eye icon, left column in lower left frame of the web
page), will not photocopy well (second icon), will be fine on an LCD
projector, laptop, or CRT (third to fifth icons), and might give problems
on a color printer (sixth icon). Further, the user can display the specifications
that should be used to print it with process colors or to display it
on the computer screen with various software in common use by people
making maps.
Animation has become a part of the arsenal of techniques in cartography,
and Jill Hallden’s population change in the U.S. is a fascinating
example (Hallden, 1997; Hallden, 1999). It shows the progression of
population across the U.S. from 1790 to 1960 and can be played through at
a constant rate or one can click on the year of interest. Figure 9 shows the
map image for 1910.
As to recent developments that are guaranteed to wow us, we only
have to go to David Rumsey’s website and look at the flyby over Yosemite
Valley as depicted on an historical map draped on a digital elevation
model (Rumsey, 2003) (Figure 10). Flying over an historic landscape is an
experience indeed.
The wonders of modern technology are also mind boggling in the new
Shuttle Radar Topography Mission results (USGS, 2003-09-25; JPL, 2003).
In just 11 days in February 2000, this mission gathered elevation data for
nearly 80% of Earth’s land area. An example product is shown in Figure
11. Think of all the effort that went into collecting and mapping elevation
since the inception of such elevation measurement and what early topographic
surveyors would say if they knew about this 11-day mission! The
form of land in areas where landform maps have never been made can
now be portrayed with remarkable detail. In a similar category of wonderment
is LIDAR, another radar technology that can be used to map even
building form (TerraPoint, 2000).
Current cartography is also represented in the maps that win awards
at the International Cartographic Association’s map exhibit. Hundreds of
current products are on display during the ICA’s biennial conference, and
in August of 2003, two of the awards went to U.S. mapmakers. NOAA
won in the mapping-with-satellite-imaging category for their global
coverage of nighttime lights (NOAA, 2000), a compilation of images based
on data from the Defense Meteorological Satellite Program. Visitors to
their website can interactively access the nighttime lights map of each
continent. Figure 12 shows an easily recognized excerpt from the North
America map.
The other U.S. award was in the CD/DVD category and went to Jim
Meacham and Erik Steiner for the electronic version of the Atlas of Oregon
(Meacham and Steiner, 2002). The still image in Figure 13 only hints at
the nature and quality of their CD-Rom, which represents the state of the
art of this type of medium. It has time sequences, rollovers, and flexible
access; and it combines these interactive features with superb attention
to the traditional aspects of maps such as color selections and mapping
methods.
Anything entitled “Cartography 2003” must also address the matter of
ICC 2003 (International Cartographic Conference and General Assembly)
held in August of 2003 with a few points about its significance. It was
the 21st such conference, and for the first time in its history was held on
the continent of Africa. ICA met in Durban, South Africa, and as you can
imagine, now that multiculturalism is the local theme instead of apartheid,
the complexion of the conference was quite different from past ones.
ICA elected its first Black African vice president (one of 7 vice presidents,
elected every 4 years), Kenyan Haggai Nyapola. And a sort of mini conferFigure
8. The layout of a Colorbrewer page. A
diverging 5-class scheme is illustrated here.
Source: Brewer, 2002. Used with permission.
(see page 66 for color version)
Figure 9. The 1910 population image in Jill
Hallden Harsha’s U.S. population animation.
Used with permission. (see page 66 for color
version)
Figure 10. An historical map of Yosemite Valley,
the current-day digital elevation model (DEM)
of the same area, and the historical map draped
over the DEM. The image in the lower right
is a scene as one flies over the area. Source:
Rumsey, 2003. Used with permission. (see page
66 for color version)
 10 cartographic perspectives Number 47, Winter 2004
Figure 11. An image created from Shuttle Radar
Topography Mission data. Source: JPL, 2003.
(see page 66 for color version)
Figure 12. The southeastern U.S. excerpted from the North
America segment of Nighttime Lights of the World. Source:
NOAA, 2000.
ence within the event resulted in something called the Durban Statement
on Mapping Africa for Africans (GOOS, 2003), with a working group
formed to continue the efforts started with that document. ICA has long
had a policy of supporting developing nations, but the relationship took a
very positive turn with the Durban conference. ICC Durban is likely to go
down in history as a highly significant event.
I have referred now to everything from enduring content in the field
of cartography to its decline as a recognized academic discipline (at least
so far as use of the term in job ads is concerned) to the marvelous access
and products that characterize cartography in 2003, which by whatever
name(s) is a thriving enterprise. Much of the content has concerned the
“whiz-bang” side of the field, but there are many things that could be described
as such and they serve as the icons of current cartography and the
benchmarks to be bettered in the future.
In looking to the future, however, I will not try to describe what the
icons will be. Rather I will return to academia for a moment. Cartography
is being done and we might say more and more of it is being done, but as
an explicit topic of courses it seems to be appearing less and less. I began
to realize recently that it is very much in the position that geography was
in within the K-12 school system for many years. Geography was absorbed
into social studies in much the same way cartography (and even
GIS) are absorbed into geographic information science, visualization, and
other descriptors. Cartography may be making somewhat of a comeback;
I find my fellow GIS instructors at MSU anyway actually wanting cartographers
to teach more cartography. But we are not going to return soon
to the identity and acceptability that we enjoyed in the early to mid ’80s.
What happens, then, to the subject matter, the content of cartography? Do
cartographic principles simply go by the wayside, to be rediscovered from
time to time? I suspect that one step we need to take is to be creative and
work it into other courses. Some of it is already taught that way, but not
always very well. To take the step of increasing the quantity and quality
of its teaching in other courses is not necessarily “giving up” on raising
interest in more explicitly cartographic courses. It could just help to spur
interest. GIS is being included across the curriculum and perhaps we need
to include more cartography across the curriculum as well--in physical
and cultural courses, in GIS and GISci, in regional geography, and perhaps
even in related other departments such as geology, resource development,
Figure 13. A screen capture of a page in the
interactive Atlas of Oregon. Used with permission.
Source: Atlas of Oregon CD-ROM,
Copyright 2002, University of Oregon Press.
(see page 66 for color version)
“ICC Durban is likely to go
down in history as a highly
significant event.”
“[we need to] work
[cartography] into other
courses.”
cartographic perspectives Number 47, Winter 2004 11
soil science, and fisheries and wildlife. The entire educational model is
changing anyway toward increased online and distance learning and
mixed models of classroom and online learning. Cartography, like other
areas, is adapting. Some good thinking into the sharing of materials, exercises,
and ideas in new and creative ways is much in order in this transition.
There are ways to involve both academics and practitioners in making
such adjustments. Practitioners have a tremendous store of knowledge
and skill that academics have no time to acquire. As modules are produced
for distance learning and mixed classroom/online learning courses,
perhaps we all have an opportunity.
To wrap up, I am both impressed with Cartography in 2003 and concerned
about where we go from here. I will close simply by stating my
opinion that NACIS has certainly grown to be one of the most important
organizations facilitating the discipline and I expect it to continue its innovative
and resourceful ways as we move forward toward Cartography
2004 and beyond.
AAG (Association of American Geographers). Various dates. Jobs in Geography,
appears in each issue of AAG Newsletter, published by AAG, 1710
16th Street NW, Washington, DC 20009.
Brewer, C. A., 2002. ColorBrewer, <www.colorbrewer.com>.
Brewer, C. A., and Suchan, T. A., 2001. Mapping Census 2000: The Geography
of U.S. Diversity. U.S. Census Bureau, Census Special Reports, Series
CENSR/01-1. Washington, DC: U.S. Government Printing Office. Also
published by ESRI Press. Web version of 2003-10-03 <http://www.census.
gov/population/www/cen2000/atlas.html>.
Cabarrus County, 2001. GIS Department Home Page,
<http://166.82.128.222/gis.html>.
Carnes, J., 2002. Map Tools: Tools for Plotting GPS Coordinates, <http://
www.maptools.com/>.
CIA (Central Intelligence Agency), 2003. The World Factbook 2003, <http://
www.cia.gov/cia/publications/factbook/>.
Curry, M. R., 1999. Rethinking Privacy in a Geocoded World, Chapter 55 in
Geographical Information Systems, 2nd ed. In Longley, P.A., Goodchild, M. F.,
Maguire, D. J., and Rhind, D. W. (Eds), 2:757-766.
Dietz, S., 1994. Lonely Planet. New York: Dramatists Play Service, Inc.
Goodchild, M., 2000. Cartographic Futures on a Digital Earth. Cartographic
Perspectives, #36, Spring, pp. 3-11.
GOOS (Global Ocean Observation System), 2003 (August 25). Mapping
Africa for Africa. <http://ioc.unesco.org/goos/Africa/mapping_Africa.
htm>.
Hallden (Harsha), J. K., 1997. The Population Density of the United States, by
County, from 1790 to 1960, an animated map.
REFERENCES
“. . . we move forward toward
Cartography 2004 and beyond.”
 12 cartographic perspectives Number 47, Winter 2004
Hallden (Harsha), J. K., 1999. Development of two and three-dimensional cartographic
animations to visualize population change, M.A. Thesis (Geography),
Michigan State University.
Harley, J. B., 1989. Deconstructing the Map, Cartographica, 26:2:1-20.
ICA (International Cartographic Association), 1999. Welcome to the ICA,
<http://www.icaci.org>, accessed October 2003.
JPL (Jet Propulsion Laboratory), 2003. Colored Height and Shaded Relief
- Corral de Piedra, SRTM Argentina Images, <http://www2.jpl.nasa.gov/
srtm//argentina.html>.
King, P. B., and Beikman, H. M. (compilers), 1974. Geologic map of the
United States (exclusive of Alaska and Hawaii), Reston, Va.: U.S. Geological
Survey, three sheets, scale 1:2,500,000.
Meacham, J. E., and Steiner, E. B., 2002. Atlas of Oregon CD-ROM, Eugene,
OR: University of Oregon Press.
NOAA (National Oceanographic and Atmospheric Administration), 2000
(October). Nighttime Lights of the World, <http://spidr.ngdc.noaa.gov/spidr/night_lights.html>.

Rumsey, D., 2003. David Rumsey Map Collection, <http://www.davidrumsey.com/>
TerraPoint, 2000. Image Gallery, on TerraPoint™ website, <www.terrapoint.com>.
Thelin, G. P., and Pike, R. J., 1991. Landforms of the Conterminous United
States: A Digital Shaded-relief Portrayal. MAP I-2206, U.S. Geological Survey,
scale 1:3,500,000.
USGS (United States Geological Survey), 2003 (November 6). About the
National Map, <http://nationalmap.usgs.gov/nmabout.html>, updated
from earlier versions.
USGS (United States Geological Survey), 2003 (September 25). Shuttle
Radar Topography Mission, <http://srtm.usgs.gov/>.
USNC-ICA. 2003. U. S. National Committee for ICA, <http://www.msu.
edu/~olsonj/ica/USNC-ICA.html>.
Vigil, J., Howell, D. G., Pike, D., Jewel, E., and Kalman, N., 2003 (September
30). A Tapestry of Time and Terrain. U.S. Geological Survey website
<http://tapestry.wr.usgs.gov/>, updated from earlier versions.
cartographic perspectives Number 47, Winter 2004 13
Mapping September 11, 2001:
Cartographic Narrative in the
Print Media
Robert R. Churchill
Department of Geography
Middlebury College
Middlebury, VT 05753
bob.churchill@middlebury.edu
Suzanne J. Slarsky
Linacre College
University of Oxford
Oxford OX1 3JA, UK
he attacks of September 11, 2001 on the World Trade Center and the
Pentagon were unprecedented in scope if not in their fundamental
nature. While the United States moved toward resurrection of Reagan’s
Strategic Defense Initiative, known popularly as “Star Wars”, and focused
its resources on sophisticated weaponry, terrorists with primitive weapons
turned commercial aircraft into guided missiles. The suddenness and
enormity of the events, coupled with the fact that so many people were
acquainted with victims of the attacks, created a sense of concern and
confusion that was more pervasive and ubiquitous than evoked by either
the 1993 bombing of the Trade Center or the 1995 attack on the Murrah
Federal Building. In the immediate aftermath, the events of September 11
attracted the sympathies of the entire country, evoked both an outpouring
of patriotism and a rhetoric of retribution, and temporarily redefined
task saliencies (Wright, 1978) as firefighters and law enforcement officers
became heroes of the moment.
The media also assumed a heightened level of importance as people
turned to television, the Internet, and print for information and for
insight and meaning. On September 11, the New York Times recorded
over 21 million page views on their site, more than twice the previous
record, and a six-month circulation audit by the Times following
September 11 showed daily gains of approximately 42,000 newspapers
(Robinson, 2002). Since the number of maps appearing in the media has
grown rapidly with the advent of desktop computing and electronic
publishing technologies (Monmonier, 1989; 2001), it is not surprising
that much of the story of September 11 has been illustrated with maps.
At the very least, these maps offer distinctive insights that help define
both the events and the public reaction, but a paradigm shift that emphasizes
their textual nature suggests that in addition to illustrating the
attacks and the subsequent events, maps cast their own narratives of
these events. Our purpose here is to explore these narratives through a
systematic examination of maps that appeared in the print media in the
period immediately following September 11.
MAPS, MEDIA, POLITICS AND PERCEPTION
Until recently, maps were most often regarded as objective, scientific
documents: mirrors of reality to borrow a familiar metaphor (Harley, 1989;
Edney, 1993; Thrower, 1996). But with the emergence of critical theory in
cartography, there no longer can be any doubt that maps are propositional,
that every map is an argument, and that maps shape our “realities” in the
same way those realities are influenced by conventional text (Harley 1988;
1989).
In this regard, the influence of popular periodicals in shaping
perspectives and points of view has been convincingly demonstrated.
Lutz and Collins (1993), for example, explored the impact of National
Geographic with special emphasis on the publication’s photographic
“. . . these maps offer distinctive
insights that help define both
the events and the public
reaction, but a paradigm shift
that emphasizes their textual
nature suggests that in addition
to illustrating the attacks and
the subsequent events, maps
cast their own narratives of
these events.”
 14 cartographic perspectives Number 47, Winter 2004
content. Because, as Sontag (1977:4) noted, “Photographed images do
not seem to be statements about the world so much as pieces of it, miniatures
of reality …”, photographs are profoundly effective in purveying
a particular world view. Photographs and maps are different, to be
sure, yet as visual forms they may share more similarities than differences,
and with digital technologies, the distinction between map and
photograph has become blurred. Add to this the fact that contemporary
society has become increasingly if not dominantly visual, and the impact
of National Geographic’s photographic reportage in shaping worldview is
even more significant.
Moreover, the combined influence of artistic images, photographs,
maps, and text almost certainly has a synergistic effect. Schulten (2001)
constructs a compelling argument that the American view of the world
– “the geographical imagination”, in Schulten’s words – in the first half
of the 20th century was the collective construct of inexpensive and widely
available maps, school atlases, and National Geographic magazine.
 With an even larger circulation than National Geographic, Readers’
Digest defined the parameters of the cold war for a substantial segment
of the American population and in so doing, demonstrated the ability of
the print media to define the Other (Sharp, 2000). Similarly, in a comparative
content analysis of newspaper reports on Bosnia and Rwanda,
Myers, Klak, and Koehl (1996) demonstrated effectively how both
rhetoric and maps were used to Other not only Rwanda but, by conflating
the entire continent, all of Africa. And defining the Other, of course,
is essential in establishing self-identity and as corollary, in promoting
nationalism and patriotism.
The structuring if not the very construction of nationalism is dependent
on cartographic representation (Anderson, 1991) to the point that it
is imperative to ask if nationalism can exist without the map. Maps have
been indispensable in efforts to foment nationalism (Herb, 1997) and
to assert national hegemony (Edney, 1997). The map was the primary
instrument in the creation of both the modern nation of Thailand and in
articulating the nation’s territory or “geo-body” (Thongchai, 1994). And
if the Israeli nation was not created cartographically, then surely the territory
was appropriated and consolidated with maps, while those same
maps were used to expunge completely the Arab presence (Benvenisti,
2000).
Perhaps because maps often serve as an icon for state unity and
control, the focus in assessing the relation between maps and nationalism
most frequently has been on the map as a significant instrument in
exercising state hegemony, a form of power knowledge. But in exploring
cartographic representations of Mother India, Ramaswamy (2001) offers
a convincing argument that geographic perceptions, and the promotion
of nationalism, in particular, can originate spontaneously in sources that
are not controlled by the state. In a similar vein, Sparke (1998) offered an
example of what he referred to as contrapuntal cartographies in which
contrasting cartographic voices both reflect and effect views that are
quite different yet in some ways complementary. While there can be no
doubt that the map historically has been and remains an essential arbiter
of political power (Harley, 1989; 1994), it is important to recognize a
more subtle reciprocity of sorts in which the map can promote nationalism
or state hegemony yet at the same time interrogate the underlying
assumptions.
The recursive character of the map more generally – the map as both
an agent-shaping viewpoint and simultaneously an expression of viewpoint
– is especially worthy of attention in exploring the media maps
“. . . the combined influence of
artistic images, photographs,
maps, and text almost certainly
has a synergistic effect.”
“The structuring if not the very
construction of nationalism is
dependent on cartographic
representation to the point that
it is imperative to ask if
nationalism can exist without
the map.”
cartographic perspectives Number 47, Winter 2004 15
that appeared in the aftermath of September 11, 2001. For while these
maps help define and articulate the events thereby molding perception
of the situation, these very same maps promote particular perspectives
and beliefs and values. Of special interest in the case of September 11
is the dynamic of this circularity: the rapidity with which perspectives
and beliefs and values shifted in the immediate aftermath. In some sense
then, media maps are a nexus of cartography, geographic self-image, and
world image.
MAPS IN THE NEWS
In an effort to better understand the reaction to events of September 11 in
general and the significance of media maps both in shaping and reflecting
the American viewpoint in the weeks that followed, we examined all
the maps that appeared in two major newspapers and three news magazines
from September 11 through October 15, 2001. The New York Times
and the Los Angeles Times were selected for their stature as major national
newspapers. At the same time, because these two papers represent different
regions and are acknowledged to have different editorial perspectives,
they may provide some insight into the relative unity or diversity
of responses politically and geographically.
Based on the assumption that weekly news magazines have time to
be somewhat more contemplative and less hurried in their reporting,
we also examined the maps in four issues each of Newsweek, Time, and
U.S. News and World Report that were published during the same 35-day
period. Due in part to greater lead time, the maps in the newsweeklies
often tend toward more elaborate compositions that are designed not
only to draw the reader into the story but to tell the story graphically
and cartographically (Ohlsson, 1988).
While there is a set of objects that nearly everyone would agree is
maps, arriving at a formal definition of a map is considerably more
difficult (Vasiliev, et al. 1990). The challenge of articulating a working
definition is further confounded by growth of electronic print media and
consequent melding of artistic renderings, photographs, and satellite imagery
with maps. Although it does not provide for unequivocal distinction
of what is and is not a map, we are partial to the definition offered
by Harley and Woodward (1987, xvi) for its breadth and inclusiveness:
“Maps are graphic representations that facilitate a spatial understanding
of things, concepts, conditions, processes or events in the human world.”
Accordingly, in this study, we include conventional maps, satellite images,
and cartoons that include maps. We also include what we refer to
as map images, i.e. map outlines or figures that are used decoratively or
for iconographic purposes.
During the five-week period of the study, a total of 193 maps appeared
in the selected publications: 152 maps in the two newspapers,
and 41 in the three newsweeklies. As evident from a tabulation of fundamental
characteristics (Table 1), maps were used frequently to provide
general geographical information, yet nearly an equal number promoted
an explicit message or position, most often through combining the map
with photographs, artistic imagery, and expository text. Over one-half
of the maps were accorded three columns or more, which undoubtedly
speaks to the media’s appreciation for their power and effectiveness.
Not surprisingly, these larger compositions were most often those of an
editorial nature, while general maps often were accorded only a single
column. The majority of maps addressed two general subjects: attacks on
the World Trade Center and the Pentagon and the search for and pros-
“In some sense then, media
maps are a nexus of
cartography, geographic
self-image, and world image.”
“. . . the maps in the
newsweeklies often tend toward
more elaborate compositions
that are designed not only to
draw the reader into the story
but to tell the story graphically
and cartographically.”
 16 cartographic perspectives Number 47, Winter 2004
SIZE
One Column Two Columns Three or More Full Page Double Page
Columns
41 54 53 37 8
SCALE2
<1:10,000 <1:100,000 <1:1,000,000 <1:10,000,000 <1:100,000,000
44 20 11 28 75
LOCATION
Manhattan Pentagon Eastern U.S. U.S. Africa/ Afghanistan World Other
Middle East
53 11 17 6 29 54 9 12
SUBJECT
Attacks and Domestic Search for International War and
Recovery Security Suspects in U.S. Terrorism Retribution
82 3 12 35 61
TYPE3
General/ Editorial/ Cartoons/
Reportorial Expository Map Images
92 86 15
SYMBOLOGY
Black-and White Color
132 61
1
What constitutes a map is not always obvious. Single compositions sometime involve more than one map: an
inset at a different scale, for example, or repetitions of the same base map to show different themes or conditions
at different points in time. Such compositions were tallied as one map. We also included satellite images, perspective
views, cartoons, and map images used for decorative or iconographic purposes.
2
For maps published without scale, the scale was determined as accurately as possible. No attempt was made to
estimate scale for cartoons and map images used ornamentally.
3
While use of map images for decorative purposes and in cartoons is apparent, the difference between general
maps used in reportorial fashion versus maps used to editorialize or promote particular arguments is far less
distinct. In this context, the first category includes maps that simply provide location reference and show accepted
features without additional comment or embellishment. The second category includes maps that represent
information subject to question or interpretation, compositions that blend maps with photographic imagery and
expository text, and maps intended to support a particular storyline rather than giving simple geographic reference.
Table 1. Selected characteristics of maps.1
ecution of terrorists in the international arena. These seemingly simple
generalizations, however, reflect more subtle temporal trends and transitions
among underlying processes.
UNDER ATTACK
In the immediate aftermath of the attacks, the media focused on attempting
to make the incredulous believable. The composition that appeared in 
cartographic perspectives Number 47, Winter 2004 17
the September 12 New York Times (Figure 1) is quite representative. Like
this one, nearly all of the maps that appeared were large-scale, detailed
representations of the target areas, but these maps also reveal a good
deal about the general mood of the country. The title alone, “Reclaiming
Lower Manhattan” belies a sense of siege, and the map not only reinforces
this impression but also delineates its areal extent. Depicted in black at
the center of the map, the wreckage of the World Trade Center defines
ground zero even before that rhetoric had found its way into the common
vernacular. Damaged buildings around this area are depicted in gray,
while toward the periphery of the composition undamaged structures are
symbolized in white. Likely intended to provide location context, these
unshaded symbols help to place bounds around the focus and suggest
the visual metaphor of a target. As lines of crippled and disabled transportation
infrastructure bound the area but also cut it into fragments, the
map further evokes some sense of an open wound. Additional metaphors
might be suggested, but what is apparent is that by its scale, focus, and
imagery, this map not only documents the devastation but also evokes
emotive reactions that color readers’ perspectives.
Although large-scale maps of the Trade Center and the Pentagon dominated
the initial cartographic coverage, a few smaller scale, regional maps
addressed collective representation of the events of September 11. Like
the map that appeared in the Los Angeles Times entitled “Morning of Terror”
(Figure 2), these regional maps showed only the eastern seaboard of
the country. While the focus is ostensibly on the geography of the events,
what is particularly obvious in this Times piece is that the body of the map
occupies less than a quarter of the frame. Moreover, the limited portion
Figure 1. Detail from “Reclaiming Lower Manhattan”, New York Times, 12 September 2001. Copyright
2001, New York Times. Reprinted with permission.
“Additional metaphors might be
suggested, but what is
apparent is that by its scale,
focus, and imagery, this map
not only documents the
devastation but also evokes
emotive reactions that color
readers’ perspectives.”
 18 cartographic perspectives Number 47, Winter 2004
of the country that is depicted is shaded in deep gray and peeks out from
the corner quite tentatively. Clearly this map was constructed hastily, and
although it almost certainly was not the deliberate intent of the mapmaker,
it is tempting to suggest that the overall effect is one of ambiguous
identity and withdrawal.
This suggestion is reinforced by similar maps that appeared in other
publications and by the fact that in the days immediately following September
11, maps and images of the continental United States were almost
non-existent, both in the formal reportage and in advertising and ancillary
material. On the one hand, this seems logical given that the attacks – both
successful and unsuccessful – all occurred along the eastern seaboard. Certainly
the distribution of these events did not require a map of the entire
country. Yet the absence of maps of the continental United States is made
conspicuous given that the outline appears on everything from mud-flaps
to advertisements for candy bars (Holmes, 1991) and has become a ubiquitous
icon that invokes a sense of belonging and identity if not allegiance
and control (Monmonier, 1989; King, 1996).
The momentary disappearance of the United States map, which is curious
at very least, is accentuated by the emphatic reappearance of the map
within a week. “One that didn’t fall”, a cartoon reprinted in the September
17 issue of Newsweek (Figure 3), depicts the continental United States as an
inviolable skyscraper impervious to dramatic attack. This cartoon heralds
not only the return of the U.S. outline to iconographic status, and its reappearance
in media maps in general, but the reemergence of the state from
its momentary period of retreat and introspection.
THE OTHER
While the devastation in New York and Washington continued to be of
interest for some time, within a week the number of maps of domestic
areas that appeared in the newspapers dropped markedly, while at the
Figure 2. “Morning of Terror”, Los Angeles Times, 11 September 2001. Copyright 2001,
Los Angeles Times. Reprinted with permission.
“The momentary
disappearance of the United
States map, which is curious at
very least, is accentuated by the
emphatic reappearance of the
map within a week.”
cartographic perspectives Number 47, Winter 2004 19
Figure 3. “One that Didn’t Fall”, Dallas Morning News, reprinted in Newsweek, 17 September
2001. Deore copyright 2001 Dallas Morning News. Reprinted with permission of Universal Press
Syndicate. All rights reserved.
same time, the number of maps of foreign locations, especially maps of
Afghanistan, increased noticeably as focus shifted from recovery, both
materialistically and emotionally, to identification of the perpetrators and
retribution (Figure 4).
Remarkably pliant, Americans’ collective geographical cognition and
perception can be stretched and reshaped as situations change (Schulten,
2001). Maps arguably serve as important agents in this process. There
may be no better example than the way in which Afghanistan emerged
as the primary culprit in the September 11 attacks. The need to define an
enemy, of course, is not only essential in seeking retribution, but defining
the Other is crucial to definition of self (Anderson, 1991). In the case of
September 11, the Othering of Afghanistan not only served to reestablish
and reassert self identity and integrity of the United States, but to contain
the enemy within a geographical border, however tenuous and permeable,
which undoubtedly contributed to a greater sense of security.
The Los Angeles Times map of September 16 (Figure 5) uses heavy, black
ink and piercing callouts to identify a number of potential targets for
retaliation, while the key at the bottom of the map attests to the United
States’ military might. The fact that these targets are identified as entire
state entities is especially apparent. The conundrum here, of course, lays
in the fact that terrorism is not state based, yet the experience of history
demands a geographical container, a discrete entity on the map, in which
to confine the enemy. Yet as a cartoon published a few days later (Figure 6)
argues, even after state boundaries were used to delineate and contain the
terrorist threat, the list was still too broad if not in terms of actual locations
of terrorist activity, then in terms of the need to clearly articulate an enemy.
Ultimately the necessary container for retribution was conveniently
provided by Osama bin Laden’s presence in Afghanistan. Also noteworthy
here is that in some 35 maps that address international terrorism, Iraq
escapes mention altogether. This seems ironic given that early justification
for military action in Iraq was based on the premise that the state was
harboring al Qaeda operatives and sponsoring terrorism, a claim that has
persisted throughout the war and occupation.
“Remarkably pliant,
Americans’ collective
geographical cognition and
perception can be stretched and
reshaped as situations change.”
“Also noteworthy here is that
in some 35 maps that address
international terrorism, Iraq
escapes mention altogether.”
 20 cartographic perspectives Number 47, Winter 2004
Figure 4. Number of maps published daily by the New York Times and the Los Angeles Times that
focused on the United States (top) compared to those that focused on Afghanistan (bottom).
Time magazine’s spread of September 24 (Figure 7) unequivocally
identifies Afghanistan as the principal foe. With the map centered on the
Mideast, the United States is nowhere to be seen and Europe is masked by
the legend, but a number of the same states that appeared in earlier maps
– Algeria, Sudan, and Pakistan, in particular – are highlighted in a muted
but visually prominent orange. Yet in spite of the content, the title “Osama’s
World”, the flashpoint symbols highlighting terrorist acts, and the
extensive text frames detailing bin Laden’s particular activities, the visual
focus of the map is Afghanistan. Advancing off the page in bright orange,
the addition of a magnified view surrounded in black leaves no visual
doubt as to Afghanistan’s sinister culpability.
By early October, the other suspects literally disappeared from the map.
There no longer was any room for cartographic equivocation as to the
identity of the enemy. Two maps further demonstrate this point: one from
the New York Times and one from Newsweek. Although stylistically quite
different, both are equally definitive and bellicose in tone. The Times map
(Figure 8) resurrects the polar projection that was so popular during the
cold war (Henrikson, 1980; 1991) to show geographical juxtaposition of
the United States and Afghanistan. The United States is located at the top
of the map, while Afghanistan occupies dead center, if not literally then
certainly figuratively, as it is the only state on the entire map distinguished
by its boundaries and encircled by radiating lines, ostensibly to show
distance and ranges but, perhaps not entirely by coincidence, resembling a
bull’s eye.
 “The Options for Battle” (Figure 9), a composition that is more elaborate
yet similar in tone, appeared in Newsweek a week later and is some-
“There no longer was any room
for cartographic equivocation as
to the identity of the enemy.”
cartographic perspectives Number 47, Winter 2004 21
Figure 5. “Potential Launch Points”, Los Angeles Times, 16 September 2001. Copyright 2001, Los Angeles Times. Reprinted with permission.
thing of a primer on cartographic representation of force (Monmonier,
1996). Suspended helplessly at the center of the map, Afghanistan is
penetrated from every direction by red arrows and completely encircled
and contained by boxes that speak with images and text to the strength,
resolve, and international scope of retaliation. Like the preceding offering
from the New York Times, as well as other maps of the period, this composition
moves well beyond Othering the enemy to foretell of inevitable
defeat. Retribution has been achieved if not in actual fact then in cartographic
construct.
 22 cartographic perspectives Number 47, Winter 2004
Figure 6. “Your brush is Too Broad”, Cincinnati Enquirer, reprinted in the Los Angeles Times, 21
September 2001. Reprinted with special permission of King Features Syndicate.
Figure 7. “Osama’s World”, Time, 24 September 2001. ©TIME, Inc. Reprinted by permission. (See
page 67 for larger color version)
GATEKEEPERS
Because the public garners its geographic information principally from
popular journalism, the media has been described as cartographic gatekeeper
(Monmonier, 1989). Even though the inherent subjectivity of atlases
and geographic reference works is now well rehearsed (e.g., Black, 1997),
media maps tend to be far more expository and far more subjective in
character. Maps in newspapers and news magazines depicted the precise
location and extent of the September 11 terrorist attacks, but they often did
so in ways that were subtly, and sometimes not so subtly, evocative and
provocative. Maps articulated locations of terrorists, but they also vilified 
cartographic perspectives Number 47, Winter 2004 23
Figure 8. “The Long Reach”, New York Times, 7 October 2001. Copyright 2001, New York Times.
Reprinted with permission.
even if it was first necessary to simplify. Maps detailed the geography of
Afghanistan, but they also expressed patriotic fervor and military resolve
in ways unlike those found in any reference atlas.
Although the view of the map that took root in the Enlightenment as an
objective, scientific, and ever more accurate representation of reality (Edney,
1999) has been recently revised if not rejected by the academy, outside
the academy the map continues to be viewed as objective and objectifying.
While school children are taught that the written word can be crafted
to present any point of view, the map is unassailable. It simply reports
factual, geographic information. The visual character of maps reinforces
this perception. Whether the map, like the photograph, can be construed
as reality rather than as representation of reality, the frequent integration
of map not only with conventional photographs but also satellite imag-
“While school children are
taught that the written word
can be crafted to present any
point of view, the map is
unassailable. It simply reports
factual, geographic
information.”
 24 cartographic perspectives Number 47, Winter 2004
Figure 9. “The Options for Battle”, Newsweek, 15 October2001. ©2001 Newsweek, Inc. All rights
reserved. Reprinted by permission. (See page 68 for larger color version)
ery (e.g., Figure 9) obscures the distinction among them and underscores
the veracity of the map. A photograph, Sontag (1977) argued, constitutes
incontrovertible proof. To many readers, the message of the map is equally
incontrovertible. So while the remarkable growth in the number of maps
in the media is a direct consequence of technology, the underlying reason
for this growth almost certainly lies in the appeal of the map, its effectiveness,
and its authority.
Like all maps, those found in newspapers and news magazines reflect
the values and viewpoints of their makers: ultimately the journalistic
institutions from which they come. While media maps reflect editorial positions,
those positions do not exist in isolation. Government, controlling
corporate interests, and editorial policies, shape journalistic perspectives
“While media maps reflect
editorial positions, those
positions do not exist in
isolation.”
cartographic perspectives Number 47, Winter 2004 25
but it also seems likely that they are affected by the social context in which
those institutions operate. Media maps then may not only shape public
opinion but arguably are influenced, even if tangentially, by that collective
opinion. What is more certain is that because all of the controlling forces
are fluid, maps document changes in prevailing influences. As the search
for those responsible for the September 11 attacks was narrowed to Afghanistan
– the “New Ground Zero”, to borrow the title from a map that
appeared in Newsweek – media maps quickly taught the public an unfamiliar
geography. And as interest in Afghanistan faded nearly as quickly as it
emerged, cartographic focus shifted to Iraq.
Beyond this general dynamic, the interplay among media interests,
government position, public perspective, and other forces suggests that
media maps often might assume different and even conflicting positions.
At one time or in one particular publication, it might be argued, a map
may promote the government position but at another time or in a competing
publication, cartography may draw that position into question.
In this context, what is striking about the maps surrounding September
11 is their remarkable similarity in tone and content across publications
recognized for differences in positions and perspectives. There simply
are few significant differences in the maps and their messages. Compare
again “The Long Reach” (Figure 8) with “The Options for Battle” (Figure
9), for example, or consider that in mid October, all three news magazines
published maps of Afghanistan, each somewhat different in design, but
all similarly large and forceful and combative in voice. Crisis tends to
generate convergence behaviors (Dacy and Kenreuther, 1969). Following
the events of September 11, there was geographic convergence of supplies
and support in all forms and an even more demonstrative convergence
of public opinion. Perhaps it is not surprising then that there also was a
convergence of cartographic representations not just of the attacks and recovery
but also in the search for perpetrators and demands for retribution.
What is less clear is whether the passage of time will give rise to divergent
cartographic voices.
Anderson, B., 1991. Imagined communities. 2nd ed. London: Verso.
Benvenisti, M., 2000. Sacred landscape: the buried history of the Holy Land
since 1948. Berkeley: University of California Press.
Black, J., 1997. Maps and politics. Chicago: University of Chicago Press.
Dacy, D. C. and Kunreuther, H., 1969. The economics of natural disasters.
New York: Free Press.
Edney, M. H., 1993. Cartography without ‘progress’: reinterpreting the
nature and historical development of mapmaking. Cartographica 30:2-3:
54-68.
Edney, M. H., 1997. Mapping an empire. Chicago: University of Chicago
Press.
Edney, M. H., 1999. Reconsidering Enlightenment geography and map
making: reconnaissance, mapping, archive. In Geography and enlightenment,
(eds) Livingstone, D. N. and Withers, C.W.J., Chicago: University of
Chicago Press, 165-198.
REFERENCES
“Media maps then may not
only shape public opinion but
arguably are influenced, even if
tangentially, by that collective
opinion.”
 26 cartographic perspectives Number 47, Winter 2004
Harley, J.B. 1988. Maps, knowledge, and power. In The iconography of landscape,
(eds) Cosgrove, D. and Daniels, S., Cambridge: Cambridge University
Press, 277-312.
Harley, J.B. 1989. Deconstructing the map. Cartographica 26:2: 1-20.
Harley, J.B. 1994. New England cartography and the Native Americans. In
American beginnings, (eds) Baker, E. W. et al., Lincoln: University of Nebraska
Press, 287-313.
Harley, J.B. and Woodward, D. (eds) 1987. The history of cartography, volume
1: cartography in prehistoric, ancient, and medieval Europe ,and the Mediterranean.
Chicago: University of Chicago Press.
Henrikson, A. K., 1980. The geographical ‘mental maps’ of American foreign
policy makers. International Political Science Review 1:4: 495-530.
Henrikson, A. K., 1991. Mental maps. In Explaining the history of American
foreign relations, (eds.) Hogan, M. J. and Paterson, T. G., Cambridge: Cambridge
University Press, 177-192.
Herb, G. H., 1997. Under the map of Germany: nationalism and propaganda
1918-1945. London: Routledge.
Holmes, N., 1991. Pictorial maps. New York: Watson-Guptill Publications.
King, G., 1996. Mapping reality. New York: St. Martin’s Press.
Lutz, C. A. and Collins, J. L., 1993. Reading National Geographic. Chicago:
University of Chicago Press.
Monmonier, M., 1989. Maps with the news: the development of American journalistic
cartography. Chicago: University of Chicago Press.
Monmonier, M., 1996. How to lie with maps. 2nd ed. Chicago: University of
Chicago Press.
Monmonier, M., 2001. Pressing ahead: journalistic cartography’s continued
rise. Mercator’s World 6:2: 50-53.
Myers, G., Klak, T., and Koehl, T., 1996. The inscription of difference: news
coverage of the conflicts in Rwanda and Bosnia. Political Geography 15:1:
21-46.
Ohlsson, I., 1988. Some comments on mapmaking in Newsweek magazine.
In Cartography in the media, (ed) Gauthier, M-J., Sillery, Québec: Press de
l’Université du Québec, 65-76.
Ramaswamy, S., 2001. Maps and mother goddesses in modern India.
Imago Mundi 53: 97-114.
Robinson. J., 2002. The New York Times Company John’s Island Club
forum. Available Online: http://nytco.com/investors-presentations-
20020325.html
cartographic perspectives Number 47, Winter 2004 27
Schulten, S., 2001. The geographical imagination in America, 1880-1950. Chicago:
University of Chicago Press.
Sharp, J. P., 2000. Condensing the cold war: Reader’s Digest and American identity.
Minneapolis: University of Minnesota Press.
Sontag, S., 1977. On photography. New York: Farrar, Straus and Giroux.
Sparke, M., 1998. A map that roared and an original atlas: Canada, cartography,
and the narration of nation. Annals, Association of American Geographers
88:3: 463-495.
Thongchai, W., 1994. Siam mapped: a history of the geo-body of a nation. Honolulu:
University of Hawaii Press.
Thrower, N. J. W., 1996. Maps and civilization: cartography in culture and
society. 2nd (ed) Chicago: University of Chicago Press.
Vasiliev, I., Freundschuh, S., Mark, D. M., Theisen, G. D., and McAvoy, J.,
1990. What is a map? The Cartographic Journal 27: 119-123.
Wright, J. E., 1978. Organization prestige and task saliency in disaster. In
Disasters: theory and research, (ed) Quarantelli, E. L., London: Sage Publications,
199-213. 
 28 cartographic perspectives Number 47, Winter 2004
Hal Shelton Revisited: Designing and
Producing Natural-Color Maps with
Satellite Land Cover Data
Tom Patterson
US National Park Service
Nathaniel Vaughn Kelso
National Geographic
This paper examines natural-color maps by focusing on the painted map
art of Hal Shelton, the person most closely associated with developing
the genre during the mid twentieth century. Advocating greater use
of natural-color maps by contemporary cartographers, we discuss the
advantages of natural-color maps compared to physical maps made with
hypsometric tints; why natural-color maps, although admired, have
remained comparatively rare; and the inadequacies of using satellite
images as substitutes for natural-color maps. Seeking digital solutions,
the paper then introduces techniques for designing and producing natural-color
maps that are economical and within the skill range of most
cartographers. The techniques, which use Adobe Photoshop software
and satellite land cover data, yield maps similar in appearance to those
made by Shelton, but with improved digital accuracy. Full-color illustrations
show examples of Shelton’s maps and those produced by digital
techniques.
Keywords: Adobe Photoshop, Blue Marble, Hal Shelton, Jeppesen Map
Company, Library of Congress, MODIS Vegetation Continuous Fields,
National Geographic Society, natural-color maps, National Land Cover
Dataset, raster land cover data, satellite images, shaded relief, The Living
Earth, Tibor Tóth, US Geological Survey.
atural-color maps are some of the most admired physical maps.
The combination of land cover colors and shaded relief brings to
the printed map a colorized portrait of the landscape that closely approximates
what people see in the natural world around them. Green
represents forest, beige represents desert, and white represents ice and
snow, and so forth. By basing map colors on the colors humans observe
everyday in nature, the goal is to create physical maps that, despite their
complex content, are easy to understand and more universally accessible
to diverse audiences.
A discussion of American natural-color maps by necessity must start
with the pioneering work of retired USGS (US Geological Survey) cartographer
Hal Shelton for the Jeppesen Map Company during the 1950s
and 60s. The first half of this paper offers a retrospective on Shelton’s
career and cartographic output, which, nearly fifty years later, still stands
as some of the finest natural-color maps ever made (Figure 1). That he
painted many natural-color maps with the apparent detail and realism
of satellite images—years before the launching of the first satellites—is a
visualization accomplishment worthy of our attention today.
The second half of the paper fast-forwards to the present day. Drawing
upon Shelton’s work as inspiration, we examine how to make naturalcolor
maps digitally from raster land cover data derived from satellite
imagery. We work with USGS National Land Cover Dataset and MODIS
Vegetation Continuous Fields, produced by NASA and the University
of Maryland. These two products detect, model, and classify land cover
INTRODUCTION
cartographic perspectives Number 47, Winter 2004 29
differently, which in turn affects the use of these data for cartographic presentation.
Step-by-step instructions and illustrations show how to create
polished natural-color maps from raw data. The design focus is on smallscale
continental maps similar to those made by Shelton.
Hal Shelton and Natural Color Maps
Shelton’s development of natural-color mapping evolved over several
decades and was not, in his words, “part of a grand design.” Nor was his
entry into the cartographic profession.
The accidental cartographer
Shelton was born in 1916 in New York State and moved with his family at
an early age to southern California, where he grew up. Today he lives in
the Rocky Mountain foothills above Denver, Colorado. His cartographic
career began in 1938 after he graduated from Pomona College, California,
with a degree in scientific illustration. Launching a career during the Great
Depression with a background in art posed a challenge for Shelton. The
only work that he could find was with a USGS field topography team conducting
plane table surveys, starting out as a rod man. Although Shelton
enjoyed mapping and working outdoors, he had other career aspirations.
After one year with the USGS, he went back to college, received a Master
of Arts degree in education, and took a teaching position with the San Diego
school district. Shelton’s brief mapping career would have ended unnoticed
at this point had it not been for the start of WWII (Shelton, 2004).
Because of his field mapping experience, during the WWII years Shelton
found himself again employed by the USGS, mapping areas considered
strategically important in the western United States. It was in the
remote Jarbridge Mountains of northeastern Nevada that Shelton, now a
full-fledged USGS topographic engineer, first began thinking about the
presentation of terrain on maps, a process that would eventually lead him
to natural colors (Shelton, 1985). Seeking place name information from the
local residents, Shelton discovered that they could not read the contour
map that he had just made. However, when he pointed across the valley
to the rugged silhouette of the Jarbridge Mountains, the residents—there
were seven in all—could readily identify Red Mountain, Old Scarface, and
the other peaks. This experience convinced Shelton that the conventional
symbology used on topographic maps was inadequate for depicting the
landscape in a manner easily understandable by general audiences. The
map symbology that he encountered was specialized and anachronistic
even by 1940s standards. For example, the USGS manual at that time specified
using a green tint for vegetation only for areas where you could hide
a small detachment of troops or nine mules. Shelton—the artist, teacher,
and by now a committed US government cartographer—was determined
to find a better way.
Becoming a terrain artist
Shelton’s subsequent government assignments took him away from the
field and to Washington, DC, Kansas, and finally to Colorado, where he
spent the remainder of his career. Working indoors now, he began experimenting
with shaded relief presentation, an effort that eventually paid
off with his appointment as Chief Cartographic Engineer for the USGS
Shaded Relief Map Program. Under Shelton’s direction the quantity and
quality of shaded relief usage on USGS maps increased. His early shaded
Figure 1. A portion of Hal Shelton’s
1:5,000,000-scale New Europe map painted ca.
1968. The original measures 107 x 137 centimeters.
Drainages and water bodies are photomechanical
additions to the original art. Courtesy
of Rand McNally & Company. (see page 69 for
larger color version)
“Seeking place name
information from the local
residents, Shelton discovered
that they could not read the
contour map that he had just
made.”
 30 cartographic perspectives Number 47, Winter 2004
relief work included large-scale maps of Yosemite Valley, California, and
Valdez, Alaska. These maps emphasized topographic form and relative
elevation by combining brown shaded relief with a green lowland tint,
overlaid with lightly printed contours. His relief presentation style during
this time was strictly conventional.
Shelton’s first attempt at natural-color mapping occurred while on temporary
duty with the US Air Force. His primary assignment was designing
aeronautical charts for use in airplane cockpits in low light conditions.
Of greater relevance to our story was another assignment redesigning
an aeronautical chart of a remote corner of the Sahara (for use under full
lighting). The replaced chart used conventional symbology—a dense
network of blue lines portrayed intermittent wadis and a green tint filled
lowland areas all but devoid of vegetation. According to Shelton, using
this chart “would tempt a pilot to land and go trout fishing.” Referring to
the realism of aerial photography, Shelton redesigned the chart to appear
appropriately arid mimicking the view seen by a pilot flying over the area.
Because lines rarely occur in nature (Shelton firmly believes in avoiding
the use of lines on maps wherever possible), the new chart depicted
wadis as light streaks across the brown desert floor. Shelton also depicted
volcanic rocks with rough-textured dark tones. The original chart based on
conventional symbology lacked a way to depict these areas, so its author
resorted to a label stating “area of dark rocks.” According to Shelton, such
text labels are evidence of a map’s failure to communicate. Nor is he keen
about legends, which he views as unnecessary on a properly designed
map. He defines a map as
“A graphic instrument of communication that transfers information
from the awareness of a person with information to the awareness of a
person without that information.”
Shelton thinks of map-making as a two-step process, each of roughly
equal importance. The first step involves the accurate gathering of data.
The second step is the depiction of that data using a “cartographic language
or vocabulary” (1985, videotape interview) —terms he uses often
and interchangeably—that others can easily understand. Map making is
also an expression of Shelton’s feelings for the land, especially wild places
“It smells different on top of a mountain than it smells down in the
valley, it sounds different at the top. As you climb… you’re getting a
tremendous amount of information.” (1985, videotape interview)
To Shelton a successful map was one that permitted another person to
“…to smell the mountain and hear the wind.” (1985, videotape interview)
Shelton would often fly over the western states with his brother, who
was a geologist and a pilot. These flights gave Shelton a firsthand impression
of the land from above, a view unimpeded by the graphical filtering
of maps. Aviation and aeronautical charting played a central role in
Shelton’s early thinking about natural-color maps. In the next phase of his
career it became even more important.
“According to Shelton, such
text labels are evidence of a
map’s failure to communicate.
Nor is he keen about legends,
which he views as unnecessary
on a properly designed map.”
“Aviation and aeronautical
charting played a central role in
Shelton’s early thinking about
natural-color maps.”
cartographic perspectives Number 47, Winter 2004 31
The Jeppesen Natural-Color Map Series
Shelton began his natural-color map career with a USGS colleague by
making freelance recreational maps of Colorado. These maps attracted the
attention of Elrey Borge Jeppesen, a United Airlines pilot who had started
a company that published aeronautical charts and other navigational
information for pilots (NAHF, 2002). He also wanted to publish general
maps catering to the ever-increasing numbers of air travelers. For the first
time the public at large was seeing Earth from directly above and Jeppesen
believed that Shelton’s natural-color maps would provide passengers with
more relevant information than conventional maps. Jeppesen and Shelton
teamed up in the early 1950s. Their business association spanned two decades
and yielded more than 30 titles in what was to become The Jeppesen
Natural-Color Map Series. The contract work for Jeppesen provided an
outlet for Shelton’s creative talents and a public forum for his cartographic
art, which received worldwide acclaim. The USGS never published any of
his natural-color maps.
Jeppesen paid Shelton by the square inch for painting natural-color
base maps. Depending on the complexity of an area, not all square inches
were equal. Any given square inch might take anywhere from one hour to
one day to complete. Initially Shelton used a Paasche AB airbrush to apply
colors, but he thought the results looked too smooth and unnatural. Painting
with 00 and 000 brushes, although slower, brought a more natural
texture to his work. However, he still used the airbrush in splatter mode to
speckle his maps with tiny green dots to represent widely dispersed trees
and brush, such as the pinyon-juniper vegetation that typifies the Colorado
Plateau. Because vegetation doesn’t generally transition abruptly in
nature, Shelton sought to depict these boundaries with soft edges on his
maps.
Since this was the era of photomechanical reproduction, painting base
maps on a stable material proved essential. One problem was seeing
underlying compiled line work after applying the first layer of paint. The
zinc plates used by the printing industry provided the solution. By etching
line work 0.05 millimeters (0.002 inches) into the plates the compilation
remained faintly visible (when illuminated obliquely) even when covered
with paint, and the etchings did not interfere with reproduction. He
would start painting by applying a white base coat to the zinc plate. He
preferred acrylics because they yielded bright white and vivid colors. Next
he painted swaths of flat color blending into one another to represent the
land cover. At this point the map was ready for the application of shaded
relief, achieved by painting light and dark tones based on each of the underlying
land cover colors. Shelton painted the land tones extending well
into water bodies with the idea that a water plate produced separately
would clip these tones at the coastline later. Lastly, the map underwent
a “balancing step” to give topographic features appropriate emphasis in
relation to one another.
Large quantities of geographic information went into making naturalcolor
maps that were easy-to-read and informative. Land cover, vegetation,
topography, geology, and climate all factored into his interpretation
of the landscape. Shelton was by no means alone in this effort. Jeppesen
hired a team of geographers to compile base line work, which guided
Shelton’s painting. Shelton and the geographic team devised a standardized
classification and colors for depicting land cover worldwide (Figure
2). Considerable discussion ensued over classifications and terminology—one
person’s scrubland was another’s shrub land, or is brush a better
term? Is chaparral a type of forest or should it be an entirely separate
“The contract work for Jeppesen
provided an outlet for Shelton’s
creative talents and a public
forum for his cartographic art,
which received worldwide
acclaim.”
“Considerable discussion
ensued over classifications and
terminology—one person’s
scrubland was another’s shrub
land, or is brush a better term?”
 32 cartographic perspectives Number 47, Winter 2004
category? The questions were as varied as the world itself. They settled on
ten categories:
Ice and snow Shrub land
Tundra Farmland (irrigated)
Evergreen forest Lava flows
Deciduous forest Sand dunes
Grassland Tropical forest
This mix of mostly vegetation zones and physiographic formations may
seem eclectic at first glance. All of the categories, however, are well suited
for distinctive depiction on a map. The goal after all was making readable
and informative maps as opposed to classifying world land cover in
a scientifically consistent manner. Thinking that too many colors might
overwhelm the reader, Shelton favored using fewer categories but the
other team members convinced him otherwise. Judging by the readability
of the finished plates, his concerns did not materialize, no doubt because
of the artistic skill he applied to the task. In a classification dominated by
nature, the inclusion of farmland is noteworthy because it acknowledges
the impact of humans on the land—a fact plainly obvious to anyone flying
over the checkerboard fields of the US Midwest. With crosshatched brush
strokes Shelton represented these field patterns on his maps. Built up areas
are the one major land cover category conspicuously absent from Shelton’s
painted bases. Admittedly, however, urban sprawl was not nearly as widespread
then as it is today. To depict urban areas on the Jeppesen maps,
bright yellow area tones were applied photo mechanically in a second step
for final printing.
Shelton/Jeppesen maps covered all areas of the globe. Uses included
wall maps and textbook maps for schools and colleges, commercial promotion,
and passenger maps for many airlines (Library of Congress, 1985).
Because of their detail and realism, NASA used these maps to locate and
index photos of Earth taken on early space missions (Figure 3).
In 1961, Elrey Jeppesen sold his firm to the Times Mirror Publishing
Company of Los Angeles but remained as president. Shelton also continued
working for the new owners until the late 1960s. In 1985, the HM
Gousha Company, a subsidiary of the Times Mirror Publishing Company,
donated 29 original plates painted by Shelton to the US Library of Congress.
The Shelton Collection, as it is now called, has grown to some 33
plates and miscellaneous other materials. Rand McNally & Company in
1996 acquired the assets of HM Gousha, which no longer exists, thereby
inheriting copyright ownership of Shelton’s plates housed at Library of
Congress (see Appendix A). The Shelton Collection can be viewed by appointment
in the Map Reading Room.
Cartographic contemporaries
Shelton’s colleagues in the cartographic profession influenced his thinking
about natural-color maps, particularly those from the Alpine countries
of Europe. The famous Walensee Map painted by Eduard Imhof in
1938, which masterfully combines land cover colors and shaded relief,
directly influenced Shelton. Shelton and Imhof met in 1958 at the 2nd
International Cartographic Conference in Chicago, the so-called “Rand
McNally” conference. At this conference Imhof praised Shelton’s natural-color
maps saying, “there is nothing more that I can contribute.”
However, the two men did not see completely eye to eye. Afterwards Imhof
visited Shelton at his studio in Golden, Colorado, for about a week.
Figure 2. Shelton’s standardized palette of
natural colors captured the character of disparate
geographic regions worldwide. Courtesy
of Rand McNally & Company. (see page 69 for
larger color version)
Figure 3. (left) Excerpt of a natural-color map
painted by Hal Shelton ca. 1968. (right) NASA
MODIS satellite image taken in 2003. Map on
left courtesy of Rand McNally & Company. (see
page 70 for larger color version)
“At this conference Imhof
praised Shelton’s natural-color
maps . . . However, the two men
did not see completely eye to
eye.”
cartographic perspectives Number 47, Winter 2004 33
According to Shelton, they politely agreed to disagree on the use of color
on physical maps. Imhof favored using color exclusively for modeling
topographic forms and depicting altitude, arguing that combining land
cover colors with shaded relief only weakens the presentation of topography.
Discussing Shelton’s work in his 1982 text, Cartographic Relief
Presentation, Imhof states
“At the small scale, however, the relief forms and the ground cover
mosaic are so finely detailed and often have so little relation to one
another that in certain areas great complexity and distortions of the
relief are unavoidable. As a result of the flatness and spaciousness of
the “models,” distinct aerial perspective hypsometric tints can scarcely
be achieved by such combinations.” (344-45)
Imhof’s point is valid—if one’s sole aim is portraying topography on
a physical map. Shelton’s approach to physical mapping, however, is
more holistic. Shelton regarded the physical world not as a cartographic
abstraction, such as elevation above sea level, but as the colors and forms
processed by his mind from reflected light observed outdoors. What was
on the terrain surface mattered as much as the terrain surface itself. If a
landscape looked arid and sun bleached, so too should its depiction on a
map.
Shelton’s closest cartographic soul mate from Europe was, perhaps,
Heinrich Berann of Austria. Like Shelton, Berann came from an art and
illustration background and painted panoramic maps lavishly adorned
with colors depicting land cover and vegetation (Patterson, 2000). Shelton
departed from Berann’s technique in using natural colors on plan maps
viewed from directly above. Although Shelton never met Berann, he
admired his work. He once had a German-speaking neighbor on vacation
call on Berann to obtain his color formulas. A page and a half of detailed
instructions on paint mixing resulted from the visit. However, Shelton
found Berann’s palette to be based more on artistic considerations than
observed nature, so he devised his own. One of Berann’s colors that did
find its way into Shelton’s palette, however, was yellow-green for depicting
humid grasslands, pastures, and meadows. Bright and decidedly unnatural,
this green occasionally detracts from Shelton’s otherwise balanced
colors, at least according to the authors’ tastes. The primitive color printing
of that time only exacerbated this problem. To be fair to Shelton, nearly
all terrain artists from that era relied on this particular shade of green;
such was the dominant influence of Heinrich Berann.
On this side of the Atlantic, Richard Edes Harrison was a contemporary
of Shelton’s in the arena of cartographic relief presentation. He
was renowned for creating artistic “over-the-horizon maps” for Fortune
magazine, and shaded relief plates containing fine physiographical detail.
Both men were similar in that they came to cartography from illustration
backgrounds, and they were both innovators and cartographic populists.
Harrison colored his maps in a conventional manner, which offered no
guidance to Shelton’s development of natural colors. However, Harrison’s
monochromatic portrayal of textured lava flows, sand dunes, and other
physiography influenced Shelton’s mapping style, which gained in detail
over the years (Shelton, 2004).
Why natural colors?
In terms of willingness to experiment with color, Shelton fell somewhere
between the conventional colors preferred by Imhof and Harrison, and the
“Shelton regarded the
physical world not as a
cartographic abstraction, such
as elevation above sea level, but
as the colors and forms
processed by his mind from
reflected light observed
outdoors.”
“In terms of willingness to
experiment with color, Shelton
fell somewhere between the
conventional colors preferred by
Imhof and Harrison, and
uninhibited end of the color
spectrum preferred by Berann.”
 34 cartographic perspectives Number 47, Winter 2004
uninhibited end of the color spectrum preferred by Berann. A half-century
ago the key players in the field of relief presentation strongly espoused
differing styles. Shelton was in the middle of this fray. The central point
of debate then—which continues today but with considerably less fervor—was
over appropriateness of hypsometric tints (colors assigned to
elevation zones). Shelton regarded hypsometric colors as “arbitrary” and
as bearing little relation to the actual color of the land, such as a green lowland
tint filling parched desert basins, and red applied to uplands where
forests grow. Classic hypsometric tints inverted the sequence of elevationinfluenced
natural colors observed by Shelton in the landscapes of the US
West (Figure 4, right).
Based on the large quantities of maps that display hypsometric tints,
an anthropologist a thousand years from now might conclude that our
society was elevation-centric. However, the current popularity of hypsometric
tints has more to do with production ease and pretty colors than it
does with our interest in elevation. Making competent hypsometric tints
requires mere minutes to accomplish with a digital elevation model and
freeware software. Even the photomechanical techniques of yesteryear
were relatively straightforward, albeit much slower. With hypsometric
tints, the end result is often a map with pleasing colors that blend softly
into one another in an orderly fashion, a design trait that people find attractive,
even if they don’t necessarily know or care about elevations. To
the average reader the elevation zone between 750 and 2,000 meters in
California, for example, which can assume any color in the rainbow on a
hypsometric tint map, is artificial, abstract, and, to use Shelton’s favorite
term, arbitrary.
 By contrast, natural colors on a map are less susceptible to misinterpretation.
For example, color-sighted humans tend to associate green with the
color of vegetation, brown with aridity, and white as the color of snow (at
least people living in the mid and high latitudes). The Nevada residents
interviewed by Shelton 60 years ago had named Red Mountain because
of its distinctive cast. Recent psychological research suggests that bright
colors attract our attention—not really a surprise—and that our memory
retention improves on images comprised of natural colors compared to
false colors or black and white (Gegenfurtner et al., 2002). Considering the
potential for natural-color maps to easily, and perhaps lastingly, communicate
geographic information to the user, why then are they so rare? The
short answer: they are tremendously difficult to make.
The making of natural-color maps manually requires that a cartographer
possess singular artistic talent, broad knowledge of physical geography,
and patience—combined traits that are in short supply, particularly
where costs are determining factors. Natural-color maps are handcrafted
and expensive products. One occasionally sees gaudy, unrefined attempts
at natural-color mapping published in tourist brochures, proof that not
everyone qualifies for the job title: artist/cartographer. Working with colored
pencils, airbrush, watercolors, and acrylics, one of the authors of this
article tried over the course of many years to create such maps, but met
with only limited success. Creating a tabloid-sized map of moderate complexity
required two to three weeks of work with the constant worry that
the airbrush could splatter without warning and ruin everything. Shelton
was considerably faster in applying pigments to maps. Providing that he
had a clear and accurate base to work from, a typical large natural-color
map would take about 40 hours to paint (Shelton, 2004).
We must also bear in mind that natural-color maps are not appropriate
for all types of general or even physical mapping. The merging of shaded
relief and land cover, regardless of how delicately done, creates a level
Figure 4. (left) A shaded relief map of southwestern
United States combined with natural
colors. (right) The same map with blended hypsometric
tints. Although hypsometric tints are
attractive and show topography clearly, they can
mislead readers about the character of the land.
Forests cover the Yellowstone region and Yuma,
Arizona, is an extreme desert environment. (see
page 70 for larger color version)
“Creating a tabloid-sized map
of moderate complexity required
two to three weeks of work with
the constant worry that the
airbrush could splatter without
warning and ruin everything.”
cartographic perspectives Number 47, Winter 2004 35
visual weight and background complexity that may detract from other
classes of information depicted on the map. Nothing good comes from
printing area colors, such as polygons showing property ownership, on
top of natural-colors, or on hypsometric tints for that matter.
Natural-color bases are suited for use with uncluttered general reference
maps and thematic maps where the surface environment and interconnectedness
matters most. They are most appropriately used at small
and medium-scales where the natural colors combine with shaded relief
to create textures that appear organic and plausibly realistic. Larger map
scales, however, require supplementary bump map textures (a type of 3D
embossment) to achieve similar results (Patterson, 2002). Although some
cartographers may be loath to admit this as a valid use, natural-color
maps make outstanding wall decorations. Even today the airline route
maps published by Jeppesen are still one of the best uses ever found for
natural-color maps.
Moving forward
Today, the foremost practitioner of natural-color mapping is Hungarianborn
Tibor Tóth, formerly an employee of National Geographic, and now
working freelance. Readers of the National Geographic Atlas of the World
would quickly recognize Tóth’s work. Showcased prominently, his painted
plates of the physical world and continents are associated by many
with the distinctive look and feel of National Geographic maps. Toth, a
talented artist and cartographer in his own right, consulted with Shelton
at his Colorado studio in early 1971. Tóth then developed a natural-color
mapping style modified and distinct from Shelton’s, which he first applied
to a map of Africa later that same year (Tóth, 1986). Instead of showing
existing land cover as Shelton did, Tóth’s maps use color to show potential
vegetation based on biogeographer AW Küchler’s data. Potential vegetation
shows readers a more abstract interpretation of the landscape without
human influences. Imagine if you will, untrammeled North America
before the first humans arrived from Asia.
Tóth—before switching to digital production—painted and airbrushed
his maps from a standardized palette formulated by carefully mixing
paints drop by drop (Tóth, 1973) (Figure 5). The National Geographic tradition
in natural-color mapping continues today thanks to Tóth’s successor,
John Bonner (Tóth also continues to work freelance for NG). Bonner’s
magnum opus was a 3.35-meter-wide globe airbrushed and painted with
natural colors that was on display at Explorers Hall in Washington, DC,
for more than a decade, up until 2000.
In 1985 the Library of Congress, Geography and Map Division interviewed
Shelton on videotape. During the 51-minute interview, Shelton
stated his hope that new people and techniques would continue the process
of natural-color mapping into the future. He concluded the interview
with the advice:
“…that we can be flexible enough to recognize change, and be wise
enough to understand those things that don’t change so much, which
is the need to have human beings to communicate.”
Turning now to the digital part of this paper, we attempt to follow his
advice.
Figure 5. (left) Tibor Tóth’s color formulas.
(right) His colors applied to a map. Courtesy
of National Geographic. (see page 71 for larger
color version)
“Natural-color bases are suited
for use with uncluttered general
reference maps and thematic
maps where the surface
environment and
interconnectedness matters
most.”
 36 cartographic perspectives Number 47, Winter 2004
Satellite Images—Seeing Things Differently
If the growth of commercial aviation compelled Shelton to paint naturalcolor
maps, going higher still, the advent of space flight largely defines the
genre today as we settle into the digital era. The digital solutions offered
in this article for making natural-color maps employ raster land cover data
derived from satellite images. But more than just a technical means to an
end, space imagery has forever changed how people visualize Earth in a
realistic context from afar. The aptly named “Blue Marble” photograph
taken by the Apollo 17 crew heading to the moon in 1972 gave us the first
full view of oasis Earth set against the emptiness of outer space (Figure 6).
Embraced as a symbol of environmental awareness, the “Blue Marble” has
become one of the world’s most recognized images. And as a natural-color
geographical image with pop culture cachet, it is a rarity.
Starting with the launch of Landsat (originally called ERTS—Earth
Resources Technology Satellite) in 1972, remotely sensed images of Earth
in both natural and false colors have become increasingly abundant with
each passing year. The sheer volume of remotely sensed data collected
by a multitude of platforms and sensors is difficult to comprehend. A
Google search in early 2004 using the key words “satellite images” yields
4,810,000 hits, a coarse, but nevertheless telling barometer of current popularity.
Satellite images have also replaced maps as the dominant method
of depicting natural color on geospatial products. However, as we shall
discuss shortly, quantity and quality are not always directly related. Offthe-shelf
satellite images are less than ideal for the presentation of natural
color on maps.
Intended primarily for scientific, military, and economic use, satellite
images find secondary (and sometimes gratuitous) application in television
and print news, posters, coffee table books, and even silk screened
on coffee mugs and t-shirts. That atlases sometimes use satellite images
and space shots of Earth as cover art points to their marketing potential.
These colorful images attract a buyer’s attention and deliver a subliminal
message that the contents within are scientifically based, accurate, environmentally
concerned, and global in scope. Perhaps for similar reasons, a
satellite image appears on the cover of the Spring 2002 issue of Cartographic
Perspectives. Satellite images also serve as fine art. The traveling exhibit
“Our Earth as Art” prepared by the Library of Congress, NASA, and
USGS is currently showing in museums and other public venues. The 41
images in the exhibit (selected from 400,000 taken since 1999) treat audiences
to eye-catching views of Earth from above that appear both surreal
and abstract (Stenger, 2002). Trying to identify the images without reading
the captions is a pleasurable challenge.
Several popular products containing global satellite data in natural
color are available today. “The Living Earth” is one of the older and better-known
commercial images (Figure 7, left). The original image derives
from 1.1-kilometer-resolution AVHRR (Advanced Very High Resolution
Radiometer) data, which NASA began collecting in 1978. While technically
sophisticated when first introduced, AVHRR products are beginning
to show their age. Contrary to what the name suggests, AVHRR images,
judged by contemporary standards, appear coarse, with too much contrast,
and contain tonal variations seemingly unrelated to land cover and
vegetation. The Living Earth, Inc. now sells updated and improved natural-color
images made from Landsat 5 and 7 data. They also have other
promising products under development.
Another notable product is NASA’s new “Blue Marble,” named after
the famous photograph taken by the Apollo 17 crew. The “Blue Marble”
Figure 6. NASA’s “Blue Marble” photograph
shows Earth from a distance of nearly 48,000
kilometers (30,000 miles). Dominated by the
Sahara and Kalahari deserts, Africa is usually
the most cloud-free continent. Antartica is also
visible in this image for the first time (NASA,
2002). (see page 71 for larger color version)
“But more than just a technical
means to an end, space imagery
has forever changed how people
visualize Earth in a realistic
context from afar.”
cartographic perspectives Number 47, Winter 2004 37
derives from MODIS (Moderate Resolution Imaging Spectro-radiometer)
data collected at 500-meter resolution and distributed to the public at onekilometer
resolution (Figure 7, right). The “Blue Marble” portrays arid regions
with convincing natural colors but is less effective in humid regions,
which appear as monotonous green tones. Meteorological interference in
the form of snow cover and sea ice in the polar regions is visible, and areas
of shallow water appear to have been arbitrarily added and appear discordant
with rest of the image.
The “Blue Marble” comes pre-composited with shaded relief generated
from GTOPO30 (Global 30-Arc-Second Elevation Data Set) that
employs standard cartographic illumination from the northwest. “The
Living Earth” image discussed previously also contains shaded relief
made from GTOPO30. While the inclusion of shaded relief is a step in the
right direction, its legibility diminishes at scales smaller than 100 percent
of original size, and, with more significant reduction, disappears entirely.
Shaded relief is not nearly as tolerant of reduction as the underlying color
tones found in satellite images. While colors tend to blend pleasingly into
one another when reduced, shaded relief becomes an unattractive dark
smudge. These shortcomings aside, the “Blue Marble” is a remarkable
dataset and is invaluable for making 3D visualizations and animations. It
is also free.
Pictures vs. maps
Based on the popularity of satellite images, many of which appear in natural
color, it would appear that Shelton’s idea about depicting Earth more
realistically has finally taken hold. Well, not quite. The graphical merging
of satellite images and maps, two closely related geospatial products,
has not happened to any great extent. It is a rare map that uses a satellite
image as a raster base in conjunction with other map information, such as
labels, lines, area tones, and point symbols. In the cartographic context,
satellite images are perhaps most common in atlases as stand-alone thematic
insets, decorative introductory pages, and the aforementioned cover
art.
The lack of satellite images used as cartographic backdrops, in the same
manner as shaded relief or hypsometric tints, has a simple explanation:
they are not maps. Remotely sensed images contain traits that are incompatible
with basic cartographic design conventions (Figure 8). Dense, detailed,
colorful, and contrasting, satellite images tend to dominate all other
classes of map information to the point of illegibility. Just as photographic
snapshots often contain undesirable visual elements—a jet contrail in the
sky above the Grand Canyon—satellite images are similarly afflicted. For
example, clear-cut forests in the US Pacific Northwest appear on satellite
images as a mottled pattern that distracts from everything else.
Other common problems with satellite images include:
Meteorological interference — Because clouds on average cover 64
percent of Earth’s surface (54 percent of land areas) at any given time,
the odds of finding satellite images completely free of clouds and their
shadows are slim at best (Warren, 1995). Even one small cloud on an
image requires a cartographer to make a difficult choice—an exercise
in cartographic situational ethics. Is it best just to leave the unsightly
blemish on the image, or is it proper to quickly remove it with the
Clone Stamp (Rubber Stamp) tool in Photoshop? After all, who would
ever notice or object? Other meteorological interference encountered
on satellite images includes snow-covered ground, frozen water bodFigure
7. (left) “The Living Earth.” (right)
NASA’s new “Blue Marble.” (see page 72 for
larger color version)
•
“Even one small cloud on an
image requires a cartographer
to make a difficult choice—an
exercise in cartographic
situational ethics.”
 38 cartographic perspectives Number 47, Winter 2004
ies, smog, smoke plumes from wildfires, and lowland flooding. Such
undesirable traits plague many of the satellite images available online
for free, which are provided by organizations that monitor the environment
and natural disasters. When using satellite images as backdrops
on maps, boring is better.
Inappropriate color variations — It is typical for natural color satellite
images to contain at least some colors that are decidedly unnatural.
Despite appearances, natural-color satellite images are not truly
photorealistic. They typically consist of “bands” of electromagnetic
radiation data from outside the visible spectrum. By inserting these
data, in the form of 8-bit grayscale images, into the red, green, and
blue channels of, say, a TIF image, the results are a concocted scene
with ersatz natural colors—if the mixing and matching of data and
colors are done correctly. The inappropriate color variations flagged
in Figure 8 are in an area of uniform desert shrub. The cause of these
pronounced variations, which are invisible to humans on the ground,
might be differing mineral or moisture content in the exposed soil—
information only of interest to a few people in specialized fields.
Regardless of what the colors represent, they are too prominent compared
to other colors on the image and do not belong on a generalpurpose
map.
Relief inversion — The southeast lighting (lower right) found on
most high-resolution satellite images when north-oriented causes an
optical illusion known as relief inversion, whereby mountains appear
as valleys, and vice versa. Because being able to tell up from down is
so fundamental, the presence of relief inversion is completely unacceptable
on maps. Removing the embedded shadows that cause relief
inversion on satellite images is technically difficult and yields poor
results, especially in areas of high, sharp relief where shadows are
entirely black. Substituting neutral colors and textures to replace the
shadows, whether by automated means or the Clone Stamp tool in
Photoshop, is an inelegant solution at best. On another tack, overprinting
the satellite image with shaded relief generated from a DEM
(digital elevation model) using standard northwest illumination only
serves to flatten and darken the topography. The shadows, based on
opposing light sources, cancel each other out.
Land cover—cartographically friendly data
In contrast to satellite images, raster land cover data derived from satellite
images is well suited for making natural-color maps. Raster land
cover data with its classified structure and generalization brings order to
the tonal anarchy of satellite images. The product of sophisticated image
processing techniques and often multiple data sources, raster land cover
data is generally free of the problems that afflict raw satellite images,
such as relief inversion. The result is reconstituted data that facilitates
the production of natural-color maps. A satellite image distilled into a
land cover classification of, for instance, a dozen categories is merely
the raster equivalent of the polygons etched into zinc plates that guided
Shelton’s painting. Shelton colored his maps with brushes and acrylic
paints. We will use software and pixels. Although the means of production
has changed, the process remains essentially the same today as it
was a half century ago—a cartographic version of painting by the numbers.
Figure 8. A natural-color Landsat image of the
Grand Canyon made from bands 2, 4, and 7.
Even the handsomest satellite images contain
graphical elements inconsistent with cartographic
design goals. Courtesy of the USGS.
(see page 72 for larger color version)
•
•
“When using satellite images
as backdrops on maps, boring is
better.”
cartographic perspectives Number 47, Winter 2004 39
Next, we will examine how to make natural color maps from two different
types of raster land cover data. Both are in the public domain and
available online for free. Our primary software for manipulating the data
is Adobe Photoshop. We will begin with National Land Cover Dataset.
National Land Cover Dataset
Produced by the USGS, National Land Cover Dataset (NLCD) is available
for the 48 contiguous states at 30-meter resolution (Figure 9). It derives
from Landsat Thematic Mapper imagery taken during the early to mid
1990s with 1992 as the oldest collection date. Coverage ends abruptly at
the borders with Canada and Mexico and seaward at the 12 nautical-mile
limit of US territorial waters.
NLCD is a type of categorical land cover data, which is the most
common variety of raster land cover data available. With categorical
land cover data, each pixel represents a sampled area on the ground
and receives a classification as one type of land cover or another. For
example, if the contents of a 30 x 30-meter sample of NLCD were 51
percent shrub and 49 percent evergreen forest, then the sample receives
the shrub assignation entirely—the winner takes all. What categorical
land cover lacks in subtlety, it makes up for in quantity. The millions of
pixels that comprise these data when reduced in scale blend land cover
colors together smoothly, a desirable trait on natural-color maps. The effect
is much like Shelton’s airbrush technique of spraying atomized color
droplets.
NLCD uses a modified form of the USGS’s Anderson Land Use and
Land Cover Classification System (Anderson et al., 1972). The full Anderson
classification system consists of four hierarchical levels and more than
one hundred categories of land cover (occupying the two uppermost levels)
and land use (occupying the two bottommost levels). The distinction
between land cover and land use is an important one. For example, forest
is a land cover category and bird watching or fire wood collecting are uses
that occur in a forest. Because determining detailed land use information
is impossible on a national dataset made from 30-meter-resolution Landsat
imagery, the NLCD classification does away with land use altogether.
It instead consists of a two-level system with nine level-one land cover
categories and 21 level-two categories (Figure 10, left).
The USGS developed NLCD for scientific and analytical tasks. Therefore,
to make natural-color maps, which are at heart artistic products,
requires a change in thinking about what the NLCD classification does.
Taking a cue again from Shelton, we next will transform the scientific
NLCD classification into an artist’s color palette (figure 10, right).
From classification to palette
The first step was reducing NLCD categories from 21 to 15 so as not to
overwhelm the reader with too much information. Because every pixel is
accounted for with categorical land cover data, reducing the number of
NLCD categories required methods other than simple deletion to avoid
the appearance of null areas on the final map.
Aggregation, a method that combines several categories as a single
generic category, was the method most commonly used. For example,
cropland in the color palette represents the aggregation of row crops, small
grains, and fallow from the NLCD classification. These detailed and temporally
sensitive agricultural categories do not contribute to our geographic
understanding on a small-scale map of the US. Reclassification was anFigure
10. (left) The NLCD classification with
USGS assigned colors. (right) The derivative
color palette used for natural-color mapping.
(see page 73 for larger color version)
Figure 9. NCLD mosaic of the 48-contiguous
states, using the USGS suggested color scheme.
(see page 73 for larger color version)
 40 cartographic perspectives Number 47, Winter 2004
other helpful method. For example, the NLCD category transitional mostly
represents clear-cut and burned forestlands in the western US. Working
under the optimistic assumption that the trees will eventually grow back,
the palette reclassifies and groups transitional with evergreen forest. Similarly,
the NLCD category urban/recreational grasses represent golf courses,
schoolyards, and other open areas found in urban environments. Reclassifying
this as low intensity development in the palette rather than as a subset
of herbaceous planted/cultivated gave discontinuous urban areas on the final
map a more concentrated appearance.
The transformation of NLCD into a palette also required the creation
of new categories. On natural-color maps the appearance of white (snow)
in lofty mountain areas tells readers that these areas are higher and colder
than adjacent lowlands. In the continental US, however, the NLCD category
perennial ice/snow occupies only scattered tiny areas in the Cascades
and northern Rockies. To give high western mountains the emphasis they
deserve, the palette contains a new category called alpine. It encompasses
all areas above timberline and slightly lower in select places, such as the
snowy and rugged Wasatch Range of Utah that barely reaches timberline.
Because the elevation of timberline varies depending on latitude, continentality,
and other factors, a DEM and biogeography references proved
essential for delineating alpine areas. The procedure involved reclassifying
all perennial ice/snow, barren, shrubland, and herbaceous/grassland as alpine for
areas above the documented timberline elevation of each mountain range
(Arno and Hammerly, 1984).
Another new palette category was desert southwest shrub. In the NLCD
classification shrubland is the largest single category, representing 18
percent of the total area of the continental US and dominating vast tracts
of the intermountain West to the exclusion of all else. The creation of the
desert southwest shrub category recognizes that not all shrublands are the
same and brings needed graphical variation to these otherwise monotonous
regions. Using a DEM to subdivide the shrubland category by
elevation zone, desert southwest shrub, which is depicted with a blush of
red, represents the hot, low-elevation Sonoran, Mojave, and Chihuahuan
Deserts of the southwestern US. The remaining area in the shrub category
primarily represents the cold sagebrush steppes of northern Nevada and
Wyoming.
Choosing colors for the palette was an exercise in subtlety. The USGS
appropriately assigned bright colors to each of the 21 NLCD categories
to make their patterns as distinct as possible. By contrast, the colors
chosen for the natural-color palette were complementary and representative
of natural environments to the greatest degree possible. With some
categories, however, graphical pragmatism dictated using conventional
map colors, such as blue for open water. The only colors in the palette not
inspired by nature are the muted purples assigned to low and high intensity
development—unnatural colors for unnatural information. The overarching
goal was to achieve a soft impressionistic portrayal of land cover that
could serve as an unobtrusive backdrop on general maps. Even though
the palette contains 15 colors, compared to ten used by Shelton, the additional
colors were not problematic because they represented land cover
categories only slightly different from one other. For example, the similar
yellowish colors depicting grassland & herbaceous and pasture & hay reflect
land cover categories with similar characteristics. If these colors happen
to merge together indistinctly in places, it is the small price that one must
pay for creating cartographic art. Not all categories deserve equivalent
strength on a natural-color map. Because trees are the most conspicuous
vegetation—they are bigger than we are—the green depicting forest on a
“The only colors in the palette
not inspired by nature are the
muted purples assigned to
low and high intensity
development—unnatural colors
for unnatural information.”
“If these colors happen to merge
together indistinctly in places,
it is the small price that one
must pay for creating
cartographic art.”
cartographic perspectives Number 47, Winter 2004 41
map deserves more prominent treatment than grassland, shrub, and other
diminutive vegetation categories. Also worthy of prominent color treatment
are land cover categories that are unique or important to humans,
such as the developed land where we dwell. In the color palette, the
emphasized colors/categories cluster at either end of the scale with muted
background colors falling in between.
Some color choices in the palette were compromises. For instance, the
light beige given to the barren category serves well at representing desert
salt flats, pale Colorado Plateau sandstones, and sand dunes, but it is
misrepresentative of lava flows comprised of dark basaltic rocks. Because
lava occupies relatively small areas that are scattered in the continental
US, this inappropriate color is barely noticeable on our map. Nevertheless,
on a future update the map needs to depict lava in a more representative
fashion. In the western US (where all the lava flows are found) sagebrush
sometimes grows abundantly on flows, which the NLCD classification
detects as shrub, obscuring their extent. The question arises: on a naturalcolor
map is it better to show lava, a geologic feature, or the vegetation
that grows on it? Considering the uniqueness of lava and ubiquity of sagebrush,
lava is perhaps the better answer. Even choosing an appropriate
color with which to portray lava presents problems—the logical choice,
gray, is easily confused with shaded relief. A possible solution is dark
red gray coupled with subtle 3D embossment and a hint of rough surface
texture.
The grouping of colors in the palette attempts to acknowledge the
non-hierarchical and interrelated character of the natural world. Although
it looks like a conventional legend, further macro level groupings exist
within the palette (Figure 11). The highest division is between the natural
and human environments. Below this level the overlapping groups contain
common colors to infer inter-categorical relationships. For example, the
group water consists of woody wetland, herbaceous wetland, and open water,
all of which contain blue in varying amounts. The color groupings, which
are invisible to the reader, bring natural order to the underlying data and
produce more harmonious colors on the final map (Figure 12).
Using NLCD in Adobe Photoshop
Having discussed what to do with NLCD, we now discuss how to do it.
First you will need to obtain NLCD, which is downloadable from two
sites maintained by the USGS (see Appendix B for URLs). The USGS
Seamless Data Distribution System provides unprojected data (sometimes
called the Geographic or Latitude/Longitude projections) for userselected
areas in either ESRI (Environmental Systems Research Institute)
compatible GRID format or as a GeoTIF. The USGS also maintains an
FTP (File Transfer Protocol) site accessible with a web browser containing
individual GeoTIF files for the 48 contiguous states in the Albers
Equal-Area Conic projection. The 30-meter-resolution data on both sites
is otherwise identical and tend to be large. To produce the map shown
in Figure 12, we used a mosaic of NLCD data of the entire contiguous
US at 240-meter resolution in the Albers Equal-Area Conic projection, an
unpublicized product. The USGS kindly gave us this 19,322-pixel-wide
TIF image via FTP in response to an email request sent from the link on
their website.
Opening NLCD in GeoTIF format in Photoshop reveals an image with a
kaleidoscope of colors similar to those shown in Figure 8. Although NLCD
may look like an ordinary RGB (Red-Green-Blue) or CMYK (Cyan-Magenta-Yellow-Black)
image, it is in indexed color mode, which is less familiar
Figure 12. California and the southwestern US
depicted with colorized NLCD and shaded relief.
(see page 74 for larger color version)
Figure 11. Color groupings in the palette. (see
page 74 for larger color version)
 42 cartographic perspectives Number 47, Winter 2004
to many cartographers. The advantage of indexed color mode over, say,
RGB color mode, is its compact file size, no larger than an 8-bit grayscale
image, and the ability to manage colors, such as those representing land
cover categories, via a color table. An indexed color table may contain up
to 255 colors.
Going to the drop menu and Image/Mode/Color Table, accesses the
Color Table dialog, where you can explore and modify the color palette.
Toggling between the presets in the Color Table (Spectrum, Mac OS
System, Windows System, etc.) vividly demonstrates how changes to the
Color Table can change the appearance of NLCD. Although the jumble
of multi-colored squares in the Color Table may look confusing at first,
their positions correspond to the numbered categories in the NLCD classification.
For example, NLCD category 11 is open water, which occupies
the 12th color square in the top row of the Color Table (counting the first
square as zero); category 43 evergreen forest occupies the 44th square;
and, so forth. If you can count, you can manage indexed NLCD colors in
Photoshop.
Changing colors in the Color Table is as simple as clicking on a square
and specifying a new color in the Color Picker or using the Eyedropper
tool to select a color from any open Photoshop image. Use the Eyedropper
tool technique to select natural colors from other maps, scanned art, digital
photographs, or any image found on-line. Stuck for a color with which
to portray desert southwest shrub? A Google photo search using the
keyword “Arizona” will yield a spectrum of choices. Or maybe a snapshot
of your golden retriever might contain the ideal color. Hint: you may need
to click the okay button to confirm your color table changes before the
Eyedropper tool works as expected between uses. Once you have chosen
new colors that you like, the modified Color Table is savable and loadable
for use with later projects and sessions (Figure 13). The Color Table used
in this project is available with the online version of this article (see Appendix
B).
Other tips for working with NLCD include:
If you plan on reprojecting NLCD with GIS or cartographic software
(NLCD is formatted to decimal degrees) use data downloaded from
the Seamless Data Distribution System. For reprojecting in GIS, GRID
(the default) or GeoTIF formats work equally well. After reprojecting
is complete, save NLCD in TIF format (with no compression) to bring
it into Adobe Photoshop. Should you find yourself with a standard
grayscale or RGB image after reprojecting NLCD, in Photoshop going
to Image/Mode/Indexed Color allows you to convert the data back
to indexed color mode. However, be aware that Photoshop randomly
generates new positions for the colors in the Color Table upon returning
to indexed color mode. Therefore, it is best to apply the final colors
via the Color Table prior to reprojecting NLCD.
Indexed color mode images in Photoshop may interpolate incorrectly
on screen with a jittery appearance at some zoom levels. If you are
not seeing what you expected, zoom in or out until the image appears
smoother.
When resampling (changing the pixel dimensions) NLCD in Photoshop,
it is key to use “nearest neighbor” interpolation to preserve the
purity of colors assigned to land cover categories. Using “bicubic” (the
Photoshop default) or “bilinear” interpolation for image resampling
•
•
•
“If you can count, you can
manage indexed NLCD colors
in Photoshop.”
cartographic perspectives Number 47, Winter 2004 43
and other transformations yields intermediate colors, which do not
respond to Color Table manipulations.
Photoshop’s functionality is limited in indexed color mode (layers and
filters, for example, are disabled). Therefore it is necessary to switch
from indexed color mode to RGB or CMYK color modes for the final
production of natural-color maps. Do this only after the application of
final colors in the Color Table in indexed color mode.
As a last step before compositing NLCD with shaded relief to make the
final map, apply a slight amount of Gaussian blur (Filter/Blur/Gaussian
Blur) to the data. Set the blur radius to 0.5 pixels as a starting point.
Applying blur softens the harsh grainy appearance of NLCD, a condition
that commonly afflicts images processed with nearest neighbor
interpolation. Because making color changes to NLCD with the Color
Table is impossible after applying Gaussian blur, as a precaution you
should use a duplicate file for this final step. Also, excluding the open
water category from blurring will preserve crisp, well-defined shorelines
and drainages.
The USGS is currently revising NLCD based on 2001-era Landsat 7
Enhanced Thematic Mapper Plus imagery. Limited areas of the US are
now available in the same classification system as the 1992 NLCD just
discussed. These upgraded land cover datasets are better able to accommodate
mixed spectral signatures across image mosaics and multiple time
captures of vegetation, which means that besides being newer, they are
more accurate. Perhaps the new NLCD will include Alaska and Hawaii,
too.
Modis Vegetation Continuous Fields
MODIS VCF (Vegetation Continuous Fields) is the second type of land
cover data that we examine. It consists of three data layers representing
forest, herbaceous, and bare land cover. Although three land cover
categories may seem scant, VCF data possesses unique qualities that are
amenable to making generalized natural-color bases. Afterwards, color
modifications and additional data are applicable to the VCF bases as
needed.
Fuzzy data
VCF is the product of two organizations. Like the “Blue Marble” discussed
earlier, it originates from the MODIS sensor aboard NASA’s Terra satellite
platform, which traverses the entire Earth every one to two days in a polar
orbit. The University of Maryland, Global Land Cover Facility created
VCF from raw MODIS data collected by NASA in 2000 and 2001. The final
500-meter-resolution land cover data derives from monthly composites
(they use seven bands of spectral information with emphasis given to
bands 1, 2, and 7) processed to remove clouds and cloud shadows (Hansen
et al., 2003).
Coverage includes all terrestrial areas of the planet except Antarctica
and the polar fringes of Canada, Greenland, and Siberia north of 80
degrees latitude. VCF land cover layers for each continent (up to several
hundred megabytes apiece) are downloadable for free from the University
of Maryland website in either the Geographic or the Interrupted Goode
Homolosine projections. If you plan on reprojecting these data, choose the
Figure 13. Using the Color Table in Adobe
Photoshop with NLCD in indexed color mode
to convert USGS colors (left) to natural colors
(right). (see page 75 for larger color version)
•
•
“Although three land cover
categories may seem scant, VCF
data possesses unique qualities
that are amenable to making
generalized natural-color
bases.”
 44 cartographic perspectives Number 47, Winter 2004
Geographic projection, which is better suited for use with most GIS and
cartographic software. MODIS VCF data layers, provided in BIL (Band
Interleaved by Line) format, readily open in Photoshop or GIS software.
Note: Photoshop only opens single-channel (i.e. grayscale) BIL files in
“Raw” file format, so make sure to note the row and column dimensions
(in pixels) prior to opening the file. Downloads also include metadata and
projection information (see Appendix B).
Unlike hard categorical land cover data such as NLCD, MODIS VCF
consists of a matrix of continuous tone values. For any given 500 x 500-
meter sample of Earth’s surface, grayscale pixels represent the three land
cover categories as percentages. Together they add up to 100 percent.
For example, Figure 14 shows forest, herbaceous, and bare land cover for
Africa loaded into the Red, Green, and Blue channels of an RGB image—a
quintessentially scientific choice of colors. A sample selected from the
relatively lush savannah of East Africa shows the content as 38 percent forest
and 62 percent herbaceous. By comparison, a sample from the Sahara
registers as 100 percent bare, as one would expect, given the extreme aridity
of that region. Elsewhere in Africa the three land cover categories blend
softly with one another much as vegetation does in nature. They also
combine to form intermediate categories. Bare desert gradually transitions
to semi-desert, semi-desert to herbaceous grassland, herbaceous grassland
to savannah, and savannah to forest. Compared to categorical land cover
data, this model better represents nature and Shelton’s painted art where
there are few stark boundaries between vegetation types.
Given the global extent, 500-meter resolution, and general nature of
MODIS VCF land cover data, this product is most appropriate for making
natural-color maps at small and medium-scales. Next, we will make a
natural-color map of North America. With diverse natural environments
ranging from tropical rainforests to ice caps, North America is a rigorous
test of the capacity of VCF for natural-color map design.
Using MODIS VCF in Photoshop
Having downloaded, decompressed, and, perhaps, reprojected VCF, you
will next need to open it in Photoshop as a raw raster file from the File/
Open dialog. Note: to open Eurasian VCF layers that are more than 42,000
pixels wide requires Photoshop CS (v. 8.0) or later. North America and
the other smaller VCF tiles are accessible to earlier versions of Photoshop,
which are limited to a maximum file width of 30,000 pixels. When opened
in Photoshop, VCF land cover appears as an ordinary 8-bit grayscale image.
White areas on the image represent open water, so in effect VCF provides
you with a bonus fourth category of information. The forest, herbaceous,
and bare information appear as grayscale values with lighter values
representing greater densities. They are analogous to photographic negatives.
This trait makes VCF amenable for use as layer masks for modulating
colors in Photoshop. After opening each VCF data layer as a separate
Photoshop file, combine them into one multilayer image as follows:
1) Create a new Photoshop document with exactly the same pixel dimensions
as the VCF data you just opened.
2) Create five new layers by selecting Layer/New Fill Layer/Solid
Color in the drop menu or by clicking the “Create a new layer” button
in the Layers palette.
Figure 14. Blended lands cover categories in
MODIS VCF. The combined values for any
sampled pixel on the map are 100 percent. (see
page 75 for larger color version)
“With diverse natural
environments ranging from
tropical rainforests to ice caps,
North America is a rigorous
test of the capacity of VCF for
natural-color map design.”
cartographic perspectives Number 47, Winter 2004 45
3) Name the layers white background, herbaceous, forest, bare and
water respectively from bottom to top.
4) Fill each layer with an exploratory color. Use white for the background,
green for forest, yellow-green for herbaceous, beige for bare,
and blue for water (these colors can be fine-tuned later in the design
process).
5) Create a layer mask for each of the layers by selecting Layer/Add
Layer Mask/ Reveal All, or by clicking the “Add layer mask” button
in the Layers palette.
6a)To insert the VCF land cover data into their respective layer masks,
copy and paste the data. Tip: you need to Option-click (Mac) or Altclick
(PC) on the Layer mask thumbnail to open the Layer mask itself
for the pasting to occur.
6b)Alternatively, you can use the Apply Image dialog (Image/Apply
Image) to insert the VCF land cover data into Layer masks (all VCF
files intended for insertion must be open). First click the Layer mask
thumbnail to activate it. Then open the Apply Image dialog and
choose one of the VCF files as the source image. The target is by default
the Photoshop file you are currently working in. Set blending to
normal and opacity to 100 percent. Repeat these steps to insert for the
two remaining VCF data files.
 7) To color the land cover layers at the full intensity as chosen in step 4,
activate the VCF layer mask for each layer mask as described in 6b
above. Then use the Levels dialog (Images/Adjustments/Levels) to
convert the grayscale data into a high contrast mask by adjusting the
Input Levels settings to 0, 1.00, and 100 respectively from left to right.
 8) To prepare the water layer, insert any one of the three VCF data files
into the Layer mask on that layer. First, activate the Layer mask. Then
use the Brightness/Contrast dialog (Image/Adjustments/Brightness/Contrast)
to convert the grayscale data into a high contrast
land/water mask by setting the contrast slider to plus 100. Lastly,
invert the mask so that water areas appear white (Image/Adjustments/Invert).
Color adjustments
When finished you should have a Photoshop file that looks similar to
Figure 15 (left side). Although preparing MODIS VCF for use in Photoshop
is complex, the resulting file permits the easy application of colors to
the data. We will start by globally colorizing the vegetation colors. Double
clicking the foreground color in the Tool palette brings up the Color Picker
and using the Fill command (File/Fill) delivers the new color to the layer
(remember to click on the Layer thumbnail before filling). Assigning new
colors to the forest, herbaceous, bare, and water layers takes only minutes.
Changing the master opacity (keep the blending mode as normal)
or manipulating the VCF grayscale data in the layer masks permits even
finer global color adjustments. For example, to bring more emphasis to
low-density forests use Curves (Image/Adjustments/Curves) to increase
the value of these areas. In the North American example, employing this
technique made the arctic tree line more distinct.
 46 cartographic perspectives Number 47, Winter 2004
Another even more powerful option is to locally adjust colors based on
environmental factors. Doing this creates new land cover categories and
adds geographically relevant color variations to the map. For example,
in Figure 15 (left side) herbaceous land cover appears as the same yellow-green
whether it shows cornfields in Iowa, rangeland in Montana, or
tundra in Nunavut. Contrast this with Figure 15 (right side), where local
color adjustments depict rangeland as yellow-gold and tundra as light
gray-green. Applying local color adjustments is technically simple—just
draw a selection boundary with the Lasso tool, apply feathering (Select/Feather)
to taste, and fill with a new color. A more critical concern,
however, is where the color adjustments are applied and the colors used.
For accomplishing this task biogeography and climatic references are a
must. Returning to the example of North American tundra, we considered
a number of geographical definitions of the arctic for delineating this
environmental zone. The 10-degree-centigrade isotherm for July average
temperature, for example, generally defines the northern limit of trees
worldwide. This definition, however, proved inadequate for subarctic
regions, such as Labrador, where tundra-like muskeg and spruce-lichen
woodland extend southward for hundreds of kilometers. To bring tundra
coloration to these deserving areas we drew the diffuse southern boundary
of the tundra zone to include the northern third of the boreal forest
zone. Because the tundra coloration applies only to herbaceous land cover,
the green forests remained undisturbed.
We applied similar environmental color adjustments to the bare VCF
layer to accentuate the polar desert of the high arctic (muted purple), alpine
areas (light gray), and the hot southwestern deserts (light red brown).
Many other local adjustments are possible. For instance, according to the
Köppen climate classification system, the 18-degree-centigrade isotherm
for January average temperature defines tropical areas in the northern
hemisphere. A slight increase in saturation for all land cover categories
within this area (delineated by a diffuse boundary) would increase the
vibrancy of tropical areas—bringing the colors on the map closer to our
perceptions of geographic reality. Considering that tropical areas in North
America account for a small percentage of the total area, as an added benefit
(and depending on the design goals of the map) brighter natural colors
could bring needed emphasis to the tropics.
Accessorizing MODIS VCF
MODIS VCF is not a complete data solution for making natural-color
maps. While the natural manner in which it blends colors into one another
is highly effective, it lacks important land cover categories one would
expect to find on a map. To bring a natural-color map based on VCF to
final completion requires supplemental data. For example, in VCF the
“bare” category does not differentiate between sand, rock, and permanent
ice and snow, etc. Any surface without biomass receives the bare classification,
be it a gravel bar in Alaska or the concrete skyscrapers of Manhattan.
Looking again at the map of North America, glaciers in northern Canada
and Greenland appear with the same white color as the surrounding bare
ground, rendering them invisible (Figure 15, left and right). Fixing this
problem involved three steps—and two days of work. First, using the
color adjustment technique discussed previously, we assigned a muted
purple tint to all bare areas (including the glaciers) in the high arctic to
depict polar desert. Next, we reprojected, rasterized, and imported DCW
(Digital Chart of the World) vector data at 1:2,000,000-scale as white polygons,
which contrasted with purple background in VCF to show glacier
Figure 15. (left) MODIS VCF in Photoshop
presented as uniform colors. (right) With
environmental color adjustments applied to the
herbaceous layer. (see page 76 for larger color
version)
“Fixing this problem involved
three steps—and two days of
work.”
cartographic perspectives Number 47, Winter 2004 47
shapes. As a final touch, in Photoshop we overprinted the white glaciers
with gray-blue shaded relief to bring tonal modulation to their surfaces
(Figure 16).
Although not applicable to our map of North America, large deserts
completely devoid of vegetation, such as the Sahara and the Rub al Khali
in Arabia, expose another problem with VCF data for the bare category.
Completely lacking in tonality, these areas appear flat and homogenous
when colorized. Furthermore, the addition of shaded relief to these areas
is often not enough to break the monotony. To bring tonal variation to the
deserts, selectively swap in the “Blue Marble” image discussed earlier in
this paper. Also derived from MODIS, the “Blue Marble” is a close kin
to VCF and merges easily with it. For best results lighten and recolorize
the entire “Blue Marble” image as desert beige using the Hue/Saturation
dialog (Image/Adjustments/Hue/Saturation). Then copy and paste the
recolorized “Blue Marble” image to replace the flat bare color in the layered
VCF file, retaining the bare VCF data as a mask. The Sahara will now
appear as a mosaic of textured dune fields and rocky areas.
Cities are another category requiring outside help on MODIS VCF.
Although VCF depicts large urban areas mostly as bare, they appear
indistinctly. For the North America map we used the USGS’s one-kilometer-resolution
Global Land Cover based on AVHRR as an add-on (see
Appendix B for URL). Placing the AVHRR city data in layer mask with an
associated layer color permitted easy adjustments to color, opacity, and
blending. Looking elsewhere, the water layer extracted from MODIS VCF
may also require replacement. While it is reasonably good for delineating
oceans and lakes, all but the widest rivers appear as discontinuous
strings of pixels. Replacing open water areas and rivers with rasterized
vector data from sources such as DCW is advisable in most cases. Doing
this also requires the removal of the embedded water from the VCF
land cover to prevent it from conflicting with the new water. We discuss
a semi-automated Photoshop technique for removing embedded water in
the next section. The portrayal of open water areas on natural-color maps
also deserves consideration. The North America map in Figure 16 portrays
open water with hypsometric tints and shaded relief derived from
ETOPO2 (2-minute Worldwide Bathymetry/Topography) data. It is ironic
that a mapping style that so stridently eschews hypsography for terrestrial
areas happens to work so well with hypsography in water areas. On the
other hand, since no human has ever seen the ocean basins without water,
portraying them with blue hypsometric tints and shading is the closest approximation
of natural colors that we have.
New MODIS VCF products are in the production pipeline based on
more recent data. The University of Maryland, Global Land Cover Facility
plans to expand the forest and herbaceous categories. Forest (woody
vegetation, more precisely), will include needleleaf and broadleaf, and
evergreen and deciduous subcategories. Herbaceous will include new subcategories
for crops and shrubs. These new data will give cartographers
even more tools to make refined natural-color maps.
Design and Production Tips
This last section provides design and production tips for making naturalcolor
maps and managing land cover data. Because of space limitations
and the ever-changing nature of software, the intent of Photoshop tips
described here is to give you design ideas and point you in the right procedural
direction. The website for this article contains additional resources
related to the discussed tips (see Appendix B). Prior experience with
Figure 16. (left) The final map primarily
based on MODIS VCF data. (right) The top
five Photoshop layers contain supplemental
data added to the MODIS VCF base. (see
page 76 for larger color version)
“It is ironic that a mapping
style that so stridently eschews
hypsography for terrestrial
areas happens to work so well
with hypsography in water
areas.”
 48 cartographic perspectives Number 47, Winter 2004
Adobe Photoshop is helpful. And given the large files involved, so too is
a graphics workstation with large amounts of physical RAM, scratch disk
space, and file storage.
Tip 1: Combining shaded relief and land cover data
Shaded relief is an essential component on all natural-color maps (Figure
17). However, the textures in shaded relief and those found in land cover,
if clumsily combined, have the potential to become heavy and messy. The
following will help you use shaded relief more effectively with land cover
data:
Show shaded relief and land cover with roughly equal emphasis.
Despite the considerable effort that goes into transforming raw land
cover data into a natural-color base, for the greater graphical good, do
not to print these colors too boldly. The same rule applies to shaded
relief. The relative visual prominence of shaded relief and land cover
varies on a map depending on viewing distance. Up close the shaded
relief appears more dominant as a dimensional texture. By comparison,
when viewed from farther away land cover colors on the map become
the more noticeable feature, appearing as broad generalized zones.
Generalize shaded relief at reduced scales. Although land cover colors
reduce to smaller sizes with no visible harm, shaded relief is not as
elastic. Excessive topographic detail at small map scales only pollutes
the background land-cover colors and detracts from our understanding
of major topographic structures. Repurposing a natural-color map
from, for instance, wall map size to textbook size requires replacing the
shaded relief with a more generalized version. As a general rule the
resolution of a DEM used to generate shaded relief should be equal to
or less than that of the land cover. For example, if a land cover image is
10,000 pixels wide, the DEM used to generate the shaded relief might
be 7,000 pixels wide. The resulting shaded relief is then upsampled
(or rendered) to 10,000 pixels wide for final compositing with the land
cover. The need for generalized shaded relief applies to all maps and
not just those with natural colors.
Remove shaded relief tones from flat areas. A typical shaded relief
contains tonal values of 10 to 20 percent density in flat lowland areas.
They serve as a neutral base upon which other topographic features,
modeled by light and shadows, project upward or downward in a
three-dimensional manner. While tones in flat areas are desirable for
stand-alone shaded relief, the overall image becomes too dark when
merged with land cover colors. A cleaner and brighter alternative is to
let the land cover colors themselves do double duty as a base tone for
the shaded relief. To do this use Curves (Image/Adjustments/Curves)
or Levels (Image/Adjustments/Levels) to clip the tonal range of the
shaded relief just enough to remove tones from the flat areas. This
procedure works best with a shaded relief possessing a full tonal range
including fine detail in the brightest highlighted slopes and densest
shadowed slopes. Be careful not to remove too much tone, or the
shaded relief will lack body and appear spindly. Using the Eyedropper
tool and the Info palette permits the removal of tones with numerical
precision.
•
•
•
“Excessive topographic detail at
small map scales only pollutes
the background landcover colors
and detracts from our
understanding of major
topographic structures.”
“A cleaner and brighter
alternative is to let the land
cover colors themselves do
double duty as a base tone for
the shaded relief.”
cartographic perspectives Number 47, Winter 2004 49
Show illumination. The illuminated slopes on a shaded relief are
almost as important as shadowed slopes. They enhance the apparent
three dimensionality of a shaded relief, giving it an embossed look
and also lightening the image. To add supplemental illumination to a
shaded relief, first create a Hue/Saturation adjustment layer (Layer/
New Adjustment Layer/Hue/Saturation). Next, copy and paste the
grayscale shaded relief into the adjustment layer’s layer mask. Then,
in the layer mask, use Curves to choke the shaded relief tonal range
so that all areas except illuminated slopes are black. For the final step
double click on the adjustment layer icon to open the Hue/Saturation
dialog and move the Lightness slider to the right until the illuminated
slopes look appropriately bright. In Figure 16, the third layer from the
top shows an illumination adjustment layer in Photoshop. A low-resolution
version of this file is available on the website of this paper for
you to download and examine.
The adjustment layer technique also works well for displaying shaded
relief shadows. The advantage is that the darkening preserves the varying
colors below. For example, forest green becomes a darker green, desert
beige becomes a darker beige, and so forth. The final result is a natural-color
map with more pure natural colors. Creating shadows with a
Hue/Saturation adjustment layer is similar to the illumination technique
described above. But this time invert the shaded relief (shadowed slopes
should be lightest) and move the Lightness slider to the left (start with settings
between –55 and –30).
Tip 2: Legend design
Despite Shelton’s misgivings about their usefulness, legends do play
an important role even on well-designed maps. Readers expect to find
legends on maps, and cartographers are partial to displaying them. If a
legend is a little redundant, that is a lesser problem than having no legend
and uniformed map-readers. Having said that, the design of legends
on natural-color maps is worthy of reexamination. The traditional map
legend explains natural colors with small, rectangular color filled boxes arranged
neatly in a row and separated from one another. Typically a black
casing line bounds these boxes. Such a portrayal disassociates the legend
colors from one another and, of greater concern, from their counterparts
on the map. If the map uses shaded relief and the legend does not, the
communication disconnect is even greater.
To improve the design of traditional legends on natural-color maps,
consider doing the following: remove the black casing lines, place the colored
boxes in a contiguous row, include shaded relief, and, perhaps, blend
the colors (Figure 18, examples 1-4). The idea is for the legend to mimic
colors on the map as closely as possible while still maintaining order. For
another step toward this goal think about using a natural legend (Figure
18, example 5). Placing legend labels on an icon of the map itself communicates
the meaning of colors directly and unambiguously to readers.
Compared to traditional legends, the disadvantages of natural legends are
that they require more space and are less tidy.
Tip 3: Mapping urban extents
Besides the making of natural-color maps, land cover data is a useful
product for mapping urban areas as a stand-alone category on general
Figure 18. (1–4) Variants of the traditional
legend. (5) A natural legend. Legend portrayal
becomes less abstract and more cartographically
realistic from left to right. (see page 77 for larger
color version)
Figure 17. Shaded relief merged with a naturalcolor
base made from MODIS VCF data. (see
page 77 for larger color version)
•
“Despite Shelton’s misgivings
about their usefulness, legends
do play an important role even
on well-designed maps.”
 50 cartographic perspectives Number 47, Winter 2004
maps. Isolating urban areas from the other categories in NLCD (and
other categorical land cover data) is simple to do with the Magic Wand
tool. Hint: set the tolerance to zero and do not choose the antialiasing and
contiguous options. Having selected the urban areas, invert the selection
(Selection/Inverse) and fill all of the other land cover categories with
white. The image should now look something like Figure 19 (upper left).
The next potential issue is one of generalization. Because the urban categories
in NLCD include transportation, depending on the scale of your data,
discontinuous roads and other stray pixels make for a noisy image. The
Median filter (Filter/Noise/Median) in Photoshop permits the removal
of unwanted pixels below a threshold of interest (Figure 19, upper right).
Moving the radius setting to the right increases the amount of generalization.
Be sure to apply the Median filter using nearest neighbor interpolation
(Preferences/General/Image Interpolation) to prevent the urban
colors from blurring.
Tip 4: Shoreline buffering
Any cartographer who has worked with data from two or more sources on
the same map is familiar with the problem of misregistration, especially
when merging raster images and vector linework. For example, matching
raster land cover data to vector water bodies often reveals unwanted
fringing pixels and data gaps—the stair-stepped pixel boundaries don’t
match smooth-edged vector shapes. This problem is solved by growing
the land cover pixels outward—a process called buffering in GIS applications—so
that they fall under the edges of vector water bodies (Dunlavey,
2002). A similar procedure is also available in Photoshop if you know
what buttons to click:
1) To start, open your land cover image in RGB or CMYK color mode
and double check that Photoshop is set to use nearest neighbor interpolation
(Preferences/General/Image Interpolation).
2) Select and delete all water pixels so they are now transparent (Figure
20, upper left).
3) Duplicate the land cover layer.
4) Select the Move tool in the Tool palette. Then on the keyboard press
the up arrow cursor once. The image will move up one pixel.
5) Merge the copied layer with original below.
6) Duplicate the merged layer and repeat steps 4 and 5. But this time
nudge the copied layer to the right.
7) Repeat this process two more times, nudging the copied layer down
and then to the left respectively.
Each cycle of copying, nudging, and merging the image grows edge
pixels outward by one pixel. Apply the steps repeatedly as needed until
all misregistration gaps disappear. Alternatively, record your steps in the
Actions palette and the save the results for one-click replays (Figure 20,
bottom). We have built a Photoshop action that automates this process. It
is available on the article website for both Mac and PC.
Tip 5: Land cover removal
Just because a category for land cover exists doesn’t mean that you have
to accept it. For example, the pixelized depiction of rivers and lakes is
often too noisy and irregular for display on a map, requiring removal
Figure 19. Using the Median filter to generalize
urban land cover data. (see page 78 for larger
color version)
Figure 20. Using the Actions palette in Photoshop
to spread shoreline pixels outward. (see
page 79 for larger color version)
cartographic perspectives Number 47, Winter 2004 51
from the raster land cover data and showing it instead with vector lines
in a drawing software application. Beyond issues of accuracy, considering
that hundreds of thousands of pixels might be water on a land cover image,
manually replacing them with the Clone Stamp tool is not a realistic
option. The Dust & Scratches filter offers a quicker and more accurate
solution. It reads the tonal contrast in an image and replaces pixels beyond
a specified threshold with nearby unaffected pixels. In Figure 21, for example,
the green forest color that dominates the image replaces the black
lakes. Follow these steps to remove imbedded water from an image:
1) Select all water pixels and fill them with black.
2) Use the Dust & Scratches filter (Filter/Noise/Dust & Scratches) to
infill the black-filled water bodies. (Be sure to use nearest neighbor
interpolation.) Experiment with different radius and threshold settings
until the black water bodies disappear. The settings will vary
depending on the contrast range in your image.
It is the authors’ hope that this paper spurs renewed interest in naturalcolor
mapping. The digital procedures presented in this paper, we believe,
will permit many more cartographers to make natural-color maps. The
combination of Adobe Photoshop software and raster land cover data now
provides a means of producing attractive natural-color maps that, dare we
say, rival those made by Shelton. But good looks are only part of the story.
Compared to manual methods, digital production yields maps of much
greater accuracy. Map-readers can confidently assume that the pixels
representing forests, fields, and fells are where they should be. New land
cover data, such as MODIS VCF, which blends land cover categories into
one another, provides readers with insights about the indistinct vegetative
boundaries found in nature. For example, the colors representing forest
and herbaceous land combine in Africa to form a third category: savannah.
The amount of blending between, say, green forest and tawny grassland
allows readers to gauge intuitively the relative vegetative content for
any given area. And the use of shaded relief provides additional insights
about the relationship of topography and vegetation. Making complex
geospatial information such as this easy for inexperienced map-readers to
grasp is what cartographic communication is all about. That natural-color
maps are also visually pleasing and attract and hold our attention only
adds to their effectiveness, in the words of Shelton, as “instruments of
communication.”
 Digital production has largely removed the economic and time disincentives
that have been associated with the making of natural-color
maps in the past. Most of the data discussed in this paper is in the public
domain and available online for free (see Appendix B). Adobe Photoshop,
although relatively expensive, is already part of the software toolkit on
many cartographers’ computers. Most of the time needed for making
natural-color maps digitally is spent in tedious data management chores:
finding and downloading large data files, converting obscure formats, and
reprojecting and registering shaded relief and land cover data. Compared
to the manual era, however, the time needed for making a digital naturalcolor
map now requires days rather than weeks or months. Once the data
is ready, the procedures and examples we have described explain how to
design and produce natural-color maps. Switching your thought processes
from data management mode to a more creative mindset is a key
to success. Because critical design decisions occur at the end of the project
when time is often running short, one must resist the temptation to rush to
completion.
Figure 21. Removing drainages from NLCD
with the Dust & Scratches filter. (see page 80 for
larger color version)
CONCLUSION
“The combination of Adobe
Photoshop software and raster
land cover data now provides a
means of producing attractive
natural-color maps that, dare
we say, rival those made by
Shelton.”
 52 cartographic perspectives Number 47, Winter 2004
 While one no longer must be an accomplished artist/cartographer to
make natural-color maps, good design sense and grounding in physical
geography are still necessary prerequisites. Because of our tendency to
overuse new design trends, cartographers must be mindful that naturalcolor
maps are not applicable to all physical mapping situations. Shelton’s
claims of arbitrariness aside, hypsometric tints are acceptable, and they
excel at showing elevation zones and topographic forms, if that is what
one wants to emphasize on a map. Cartographic choice is a good thing.
Hal Shelton revisited: returning to art
Because this article began with a discussion of Shelton’s early years in
cartography, it is fitting that it should end with a few words about his later
career. Art has become increasingly important in his life. After easing out
of his relationship with the Jeppesen Map Company, Shelton turned his
attention to painting ski area panoramas. His work included many of the
major resorts in North America and a panorama of Grenoble, France, used
by ABC TV for the 1968 Olympics. For his most famous panorama, “Colorado:
Ski Country USA,” Shelton received a lifetime ski pass to all resorts
in Colorado, which he has put to good use for decades.
Shelton now devotes his time to painting—a return to his early art
interest before it was interrupted by some 40 years of cartography. When
creating art, Shelton finds that he is
“responding to a broader spectrum of realities than I was able to do in
cartography.”
The artistic and cartographic careers of Shelton came full circle in 1985
with an unusual request from the U.S. Library of Congress. The Geography
and Map Division commissioned him to paint a landscape using the
techniques he learned as a natural-color cartographer. Having applied
his art training to cartography for so many years, the idea was for cartography
to give something back to art. The result was “Canyon Lands,” a
1.9-meter-wide triptych displayed behind the reference desk in the Map
Reading Room (Figure 22). Although at the time of this writing “Canyon
Lands” no longer is on display, you may view it privately by asking one of
the librarians. The effort is worthwhile. Shelton’s “Canyon Lands” repays
cartography’s debt to art most generously.
Just as the making of natural-color maps is a team effort, so too is the
writing of an article about them. The authors wish to thank the following
people for their kind assistance: John Hutchinson, USGS EROS Data
Center; Marc Weinshenker, Angie Faulkner, Mark Muse, Melinda Schmitt,
and Ed Zahniser, US National Park Service; Christine Bosacki, Nystrom;
Bernhard Jenny, ETH Zürich; Linda Schubert, Rand McNally & Company;
Jim Flatness and Ronald Grim, US Library of Congress; Tibor Tóth; and,
most importantly, Hal Shelton and his wife Mary.
Anderson, J. R., Hardy, E. E., and Roach, J. T., 1972. A Land-use Classification
System for use with Remote-sensor Data. U.S. Geological Survey
Circular 671.
Arno, S. F. and Hammerly, R. P., 1982. Timberline: Mountain and Arctic Forest
Frontiers. The Mountaineers, Seattle.
Figure 22. “Canyon Lands” by Hal Shelton.
Millard Canyon, Utah, dominates the center of
the scene and the snowcapped LaSal Mountains
are faintly visible on the right horizon. The
vertical triptych joints do not appear because
of digital compositing. Courtesy of Library of
Congress. (see page 80 for larger color version)
ACKNOWLEDGEMENTS
REFERENCES
cartographic perspectives Number 47, Winter 2004 53
Gegenfurtner, K. R., Sharpe, L. T., Wichmann, F. A., 2002. The Contributions
of Color to Recognition Memory for Natural Scenes. Journal of Experimental
Psychology, American Psychological Association, Inc., Learning,
Memory, and Cognition 2002, Vol. 28, No. 3: 509–520.
Dunlavey, P., 2002. GRASS - A Useful Tool for the Mountain Cartographer.
2002 ICA Mountain Cartography Workshop Proceedings.
 http://www.karto.ethz.ch/ica-cmc/mt_hood/abstracts/dunlavey.html

Hansen, M. C., DeFries, R. S., Townshend, J. R. G., Carroll, M., Dimiceli,
C., and Sohlberg, R. A., 2003. MOD44B: Vegetation Continuous Fields Collection
3, Version 3.0.0 User Guide. http://modis.umiacs.umd.edu/documents/MOD44B_User_Guide_v3.0.0.pdf
Imhof, E., 1982. Cartographic Relief Presentation, In Steward, H. J. and de
Gruyter, B. (Eds) New York: 344-45.
Library of Congress. Circa 1985. Brochure announcing exhibit of the Shelton
Collection.
NAHF (National Aviation Hall of Fame). Circa 2002. Website about Elrey
Jeppesen. http://www.nationalaviation.org/museum_enshrinee.asp?erai
d=5&enshrineeid=375
NASA. 2002. Apollo 17 Anniversary: Celebrating thirty years of Earth-observing.
Goddard Space Flight Center. Website. http://www.gsfc.nasa.gov/
topstory/2002/1203apollo17.html
Patterson, T., 2000. A View from on High: Heinrich Berann’s Panoramas
and Landscape Visualization Techniques for the U.S. National Park Service.
Cartographic Perspectives. 36: 38-65.
Patterson, T., 2002. Getting Real: Reflecting on the New Look of National
Park Service Maps. Cartographic Perspectives, 43:43-56.
Shelton, H., 1985. Video interview. Introduction by John Wolter, Chief, Geography
and Map Division, Library of Congress. Interview conducted by
Thomas K. Hinckley (BYU). Produced by TV Facilities of Motion Picture
Broadcasting & Recorded Sound Lab, Library of Congress. 51 minutes.

Shelton, H., 2004. Personal communication with authors.
Stenger, R., 2002. From distant eye, Earth becomes art. CNN website. http://
www.cnn.com/2002/TECH/space/12/06/satellite.art/
Tóth, Tibor. 1973. Terrain Representation Manual, Cartographic Division,
National Geographic Society.
Tóth, T., 1986. Moving on! Karlsruher Geowissenschaftliche Schriften. Christian
Hermann and Hans Kern (publishers).
Warren, S., Circa 1995. Global Cloud Climatology from Surface Observations.
http://www.gcrio.org/ASPEN/science/eoc94/EOC1/EOC1-25.html
 54 cartographic perspectives Number 47, Winter 2004
Africa World: Eastern Hemisphere Palestine
Acrylic on zinc (1961) Acrylic on zinc (undated) Acrylic on zinc (1965)
Lambert Azimuthal Equal-Area Projection Parabolic Equal-Area Interrupted Projection Lambert Conformal Conic Projection
Scale: 1:6,336,000 Scale: 1:25,344,000 Scale: 1:1,000,000
163 x 131 cm 93 x 98 cm 91 x 60 cm
Arizona World: East Asia Gore Nevada
Acrylic on zinc (1955) Acrylic on zinc (1957) Acrylic on zinc (undated)
Lambert Conformal Conic Projection Parabolic Equal-Area Non-Conformal Proj. Lambert Conformal Conic Projection
Scale: 1:1,000,000 Scale: 1:25,344,000 Scale: 1:1,000,000
86 x 75 cm 91 x 65 cm 92 x 65 cm
California Color Legend Reno Area
Acrylic on paper-covered zinc (1959) Acrylic on zinc (1957) Acrylic on paper-covered zinc (undated)
Lambert Conformal Conic Projection 18 x 29 cm Lambert Conformal Conic Projection
Scale: 1,000,000 Scale: 1:250,000
72 x 58 cm
Colorado Los Angeles Area Salt Lake City Area
Acrylic on zinc (1957) Casein on paper-covered zinc (1949) Acrylic on paper-covered zinc (1950)
Lambert Conformal Conic Projection Lambert Conformal Conic Projection Lambert Conformal Conic Projection
Scale: 1:1,000,000 Scale: 1:500,000 Scale: unknown
48 x 65 cm 51 x 56 cm 53 x 39 cm
Eurasia Middle East South America
Acrylic on zinc (1961) Acrylic on zinc (1951) Acrylic on zinc (undated)
Lambert Azimuthal Equal-Area Projection Lambert Conformal Conic Projection Parabolic Equal-Area Projection
Scale: 1:9,504,000 Scale: 1:5,000,000 Scale: 1:5,000,000
122 x 150 cm 102 x 123 cm 183 x 118 cm
Europe World Southern Hemisphere
Acrylic on zinc (1957) Acrylic on paper-covered zinc (1951) Acrylic on zinc (1962)
Lambert Azimuthal Equal-Area Projection Mercator Projection Polar Orthographic Projection
Scale: 1:12,038,400 Scale: 1:31,400,000 Scale: 1:1,000,000
51 x 61 cm 102 x 157 cm 65 x 61 cm
New Europe North America South Polar Area
Acrylic on zinc (undated) Acrylic on zinc (1956) Acrylic on zinc (1957)
Lambert Conformal Conic Projection Lambert Azimuthal Equal-Area Projection Polar Stereographic Projection
Scale: 1:5,000,000 Scale: 1:6,336,000 Scale: 1:25,344,000
107 x 137 cm 145 x 119 cm 48 x 53 cm
Western Hemisphere Northern Hemisphere United States
Acrylic on illustration board (1962) Acrylic on zinc (1963) Acrylic on paper-covered zinc (1956)
Global Projection Polar Orthographic Projection Lambert Conformal Conic Projection
Scale: unknown Scale: unknown Scale: 1:3,000,000
58 x 44 cm 56 x 57 cm 102 x 175 cm
Eastern Hemisphere New Mexico Utah
Acrylic on illustration board (1957) Acrylic on zinc (undated) Acrylic on zinc (1956)
Global Projection Lambert Conformal Conic Projection Lambert Conformal Conic Projection
Scale: unknown Scale: 1:1,000,000 Scale: 1:1,000,000
43 x 33 cm 79 x 71 cm 91 x 65 cm
Hawaiian Islands North Polar Area Wyoming
Acrylic on zinc (1957) Acrylic on zinc (1956) Acrylic on paper-covered zinc (1953)
Lambert Conformal Conic Projection Polar Stereographic Projection Lambert Conformal Conic Projection
Scale: varies depending on island Scale: 1:25,344,000 Scale: 1:1,000,000
65 x 89 cm 55 x 73 cm 48 x 65 cm
World: Western Hemisphere Pacific Northwest
Acrylic on zinc (undated) Acrylic on paper-covered zinc (1962)
Parabolic Equal-Area Interrupted Projection Lambert Conformal Conic Projection
Scale: 1:25,344,000 Scale: 1:1,000,000
93 x 75 cm 117 x 183 cm
Appendix A: Shelton Collection, Library of Congress
cartographic perspectives Number 47, Winter 2004 55
Appendix B: Internet Resources
The website for this article
http://www.nacis.org/cp/cp45/shelton/index.html
Satellite images
NASA “Blue Marble” (free)
http://earthobservatory.nasa.gov/Newsroom/BlueMarble/
Landsat images of the World (free)
https://zulu.ssc.nasa.gov/mrsid/mrsid.pl
NASA MODIS (free)
http://modis.gsfc.nasa.gov/gallery/index.php#
The Living Earth (commercial)
http://livingearth.com/
Worldsat International, Inc. (commercial)
http://www.worldsat.ca/
Land cover data
University of Maryland, Global Land Cover Facility
MODIS Vegetation Continuous Fields (free)
http://modis.umiacs.umd.edu/vcfdistribution.htm
USGS National Land Cover Dataset (free)
http://landcover.usgs.gov/natllandcover.asp
http://seamless.usgs.gov/
USGS Global Land Cover (free)
http://edcdaac.usgs.gov/glcc/glcc.html
European Commission Global Land Cover (free)
 http://www.gvm.jrc.it/glc2000/Products/fullproduct.asp
Boston University Global Land Cover (free)
http://duckwater.bu.edu/lc/mod12q1.html
GAP Analysis Program (free)
http://www.gap.uidaho.edu/Projects/FTP.htm
Elevation data
NOAA ETOPO2 Worldwide Bathymetry (free)
http://www.ngdc.noaa.gov/mgg/image/2minrelief.html
USGS GTOPO30 Global Topographic Data (free)
http://edcdaac.usgs.gov/gtopo30/gtopo30.html
SRTM - Shuttle Radar Topography Mission (free)
http://www2.jpl.nasa.gov/srtm//pub_dist.htm
Artist/Cartographers
Tóth Graphix Cartographic Art Studio
 http://www.tothgraphix.com/
 56 cartographic perspectives Number 47, Winter 2004
cartographic
techniques
Figure 1. (see page 81 for color version)
Figure 1a. (see page 82 for color version)
Note from the Editor: the content of
this paper was presented at the Annual
Meeting of the North American
Cartographic Information Society,
Jacksonville, FL, October 2003
Small Type, Screens and Color
in a PostScript Offset Printing
Environment
Nat Case, Head of Production
Hedberg Maps, Inc.
Minneapolis, MN
ncase@hedbergmaps.com
As with any map design project,
a conventionally printed map (i.e.
halftone-tinted, offset-printed), is
easier to create if a wide variety of
colors are available for all aspects
of the map. One place where this
becomes challenging is in small
type and symbols (for this paper
symbols and type are considered
the ‘same’). In order for type to be
legible, sense dictates it be printed
in a solid color, as half-tone screens
will render most small shapes illegible.

Logically then, a cartographer
will fall back on solids of the inks
he/she will be printing in, to determine
what colors are available
for small type. If one is printing
in the standard CMYK process
universe, this means three solid
colors: black, which is fine; cyan,
which is not the best possible blue
but is certainly acceptable, and
magenta, which is an unpleasant,
acid color. No green, no brown, no
gray….etc.
Historically, if a cartographer
wants a good range of colors, it is
useful to think in terms of printing
outside the standard process
palette of cyan, magenta, yellow,
and black. Many national survey
series are based on such alternate
palettes, like the USGS 7.5’ series’
familiar black, green, red, blue,
brown, and purple.
Another alternative is to adopt
an alternate four-ink process palette.
In the mid-twentieth century,
many U.S. oil company maps
adopted a palette consisting of a
light blue similar to cyan, a warm
red instead of magenta, a yellow
somewhat redder than modern
process yellow, and a dark blue
instead of black (Figure 1 is an
example, a section from a Gousha-published
Gulf Oil map of
New Jersey form 1942). Though a
desirable palette, Hedberg Maps
adopted a different palette early
in its publishing history that
allowed for a somewhat larger
range of colors, and was closer
to CMYK for proofing purposes.
This palette was also observed
in the late Falk-Suurland’s line:
process cyan and black, combined
with a warm red and reddish
yellow.
The disadvantages of using
such a non-CMYK process palette
are more modest than a totally
spot-color-dominated palette,
but do include (1) incompatibility
with off-the-shelf proofing
and color-calibration systems, (2)
problems integrating photography
or client-supplied artwork,
and (3) a modest additional expense
from printers (especially for
short print runs) involving press
wash-up and ink costs.
In theory, of course, mapmakers
can approximate most colors
by using combinations of the
modern process palette. The
problem is that virtually all colors
involve screen tints and these, as
mentioned above, do not generally
support the shapes of letterforms
at small sizes—or that, at
least, is the theory.
In the last few years, Hedberg
Maps has switched to standard
CMYK, and has maintained a relatively
broad range of small type
colors, by adhering to the principles
and techniques outlined
in this paper. These principles
and techniques depend on good
registration, so lower-end map
printing applications need to adjust
these rules judiciously. It has
been noted that several other map
companies are engaging in similar
techniques, therefore this paper
should spark further creative
thinking about color and type.
1. Use The PostScript Edge
PostScript defines all letterforms
by their outline. This hard edge is
maintained all the way through
processing a vector-based PostScript
file, to the point where it is
rasterized for the output device.
Among other things, this means
that halftone screen dots that cross
the hard line will be cut cleanly
along the line, maintaining the
shape rather than distorting the
edge fluidly towards the dot. If
output is direct to plate, this hard 
 Number 47, Winter 2004 cartographic perspectives 57
edge will be first-generation when
it hits paper.
This is dramatically different
from older manual techniques,
where—especially over the life
of multiple revisions—you could
count on small but cumulatively
significant optical distortions
related to the overall set of shapes
and their interactions, not to the
specific shape objects. Figure 1
shows how the photo-film process
caused water type in the same ink
as the green tint behind it to meld
with the blue screen portion of that
tint.
2. Solid Ink Plus Screened Ink
A solid of one color is sufficient to
hold a shape if open tints of a second
color are added. Open tints
are those formed by solid dots in
an open space, i.e. screens under
about 55% using most algorithms.
The theoretical screen percentage
where dots in a square grid would
touch is 78.5%, but the convention
is to switch positive and negative
somewhere between 50% and
60%, so that in darker tints the
pattern is not dark dots against
paper, but paper dots reversed out
of a solid ink If closed tints (those
formed by open circles within a
solid ink) are added to a solid of
another color, there is a risk that
in the event of off-registration,
both inks trying to hold the letterform
shape will compete and accentuate
the registration problem.
By placing a series of dots against
a solid of another color, you allow
the solid to hold the shape, while
the “shapeless” mass of dots in
another ink only adds color.
3. Dark Ink Plus Light Ink
Most cartographers already so this
to create greens and warm reds.
In the CMYK world, yellow can
be added in any screen percentage
(including 100%) with nearimpunity
to any solid dark ink.
If you are using non-CMYK inks,
any light ink will have the same
effect. To judge an ink’s lightness,
consider its “L” value in the L*A*B
color system. For example, in Photoshop
(where one can determine
L*a*b values in the color picker)
black has an L*a*b lightness value
of 0, cyan of 62, magenta of 48 and
process yellow of 94 (Kennelly and
Kimerling, 2003).
4. Solid Type Against Screened
Line Work Not Including That
Ink Color
Running any solid dark ink type
against an open screen of another
dark ink (or inks) works well,
because the edge of the solid dark
type remains intact. For many of
our street maps, Hedberg Maps
use a street line that’s a combination
of cyan, yellow and magenta.
When street labels are printed in
black ink, the line work and the
type do not have to be kept totally
separate (though of course it looks
even better when they are kept
separate).
This is not a carte blanche.
One still can’t print dark type
over linework with too strong
a contrast. As a principle, the
contrast between your type and
your overprinted linework should
be greater than that between the
linework and the background.
Figure 2 illustrates two editions
of Hedbergs Maps’ Cambridge
Street Map title. On the left is the
first edition as printed in four spot
colors. On the right, a modified
process palette meant creating a
street line color as a tint combination.
This illustrates that the street
line work was too dark.
5. Solid Type Against a Screen
of the Same Ink Color: Text Line
Weight should be at an appropriate
ratio to the dot diameter.
Solid type can be placed in some
situations against tints of the same
ink color, but as the type gets
smaller, the potential for legibility
Figure 2. (see page 83 for color version)
Figure 3a. (see page 83 for color version)
Figure 3b. (see page 84 for color version)
problems gets greater. The letter
shapes begin conflicting with the
dots of the screen pattern. What
appears to happen, in part, is that
the reader’s eye wants to average
the dot pattern out as gray, but
doesn’t know what to do when a
dot is also affecting the shape of a
small piece of type. The end effect
is often one where some pieces of
letters seem to disappear as they
move in line with rows of dots,
and where other pieces of type
become “filled in” as other dots
close gaps and breaks in letter
shapes.
An initial subjective look at
samples, for example in Figure 3,
seems to indicate that the determining
factor is dot size versus the
width of lines forming the letter 
 58 cartographic perspectives Number 47, Winter 2004
form. This makes sense intuitively:
if a dot is dramatically smaller
than a line it overlaps, it may not
read as a related shape, whereas
if a screen dot approaches the
density of a shape within a letterform,
it may become hard to
tell them apart when reading or
scanning at full speed. Of course,
other factors creep in to affect
legibility: small type in general
can be hard for many to read, and
fonts in themselves can be easier or
harder to read depending on size,
and at very small point size (under
6 point, and especially under 5
point) great care must be taken in
choosing fonts for legibility.
To use this principle, it helps to
have an idea of dot size and the
width of lines within letters. To
calculate dot diameter d, given a
screen frequency f in dots per inch
(dpi) and a tint percentage p, the
relationship can be initially stated
as:
This can be reduced to:
This formula will give ideal,
theoretical dot size. RIPs and output
devices distort this in order to
make up for dot gain, and once on
press dot gain can make a mockery
of the numbers thus derived. Nevertheless,
Figure 4 is a table giving
ideal dot diameters in points for
common tints and screen frequencies.

The easiest way to calculate
the line weight of a font is to set a
piece of type at 10 points, and onscreen
to draw a line that matches
a thin line within the test. The
Screen percent 50 dpi 72 dpi 100 dpi 133 dpi 150 dpi 200 dpi
5% 0.36 0.25 0.18 0.14 0.12 0.09
10% 0.51 0.36 0.26 0.19 0.17 0.13
15% 0.63 0.44 0.31 0.24 0.21 0.16
20% 0.73 0.5 0.36 0.27 0.24 0.18
25% 0.81 0.56 0.41 0.31 0.27 0.2
30% 0.89 0.62 0.45 0.33 0.3 0.22
40% 1.03 0.71 0.51 0.39 0.34 0.26
50% 1.15 0.8 0.57 0.43 0.38 0.29
Figure 4
crossbar of the small letter “e” is a
good example, or the crossbar of a
capital “T”. One can then multiply
by one-tenth the actual text (i.e.
line weight of 6 point type will be
.6 times the 10 pt weight).
A few basic examples: Adobe’s
Helvetica Regular has a minimum
line weight at 10pt of .69
pt and Helvetica Bold is .91 pt.
In Hedberg Maps’ “house font”,
Avenir, the weights are: Book, .54
pt; Roman, .66 pt; Medium, .72 pt;
Heavy, .89 pt; and Black, 1.02 pt.
The acceptable ratio of letter line
weight to dot diameter is really an
individual decision. In different
situations, a reasonable lower limit
is somewhere between 1:1 and
1.5:1. For Helvetica then, if 1.5:1 is
chosen as the threshold, then using
the chart above, at a 133dpi screen,
6 point type (with a line weight of
about .6 x .69 pt = .41) could offer
nothing lighter than about a 22%
tint (with a dot diameter somewhere
around .28 pt), and a 10%
tint (dot diameter of .19pt) would
work with type no smaller than 4
pt (line weight of .28pt).
Serifs make the calculations almost
impossible at a numeric level.
Instead, a variant on the test performed
in Figure 4 is suggested.
Here a 50dpi screen series is laid
against 10pt type for various fonts.
Working at this large scale will
allow a few things: first, what will
be discerned is what the acceptable
dot-font size ratio is, without
regard to whether the type works
well in general at a small size.
Second, the test can be performed
without utilizing expensive highend
output; 50 dpi screens have
pretty accurate dots on most modern
laser printers, though some
laser printers (e.g., Xerox) tend to
make dots form into diamonds as
they approach 50%.
As an example, if on such a test
you decide that a 50dpi screen
of 20% is as dark a screen as can
be tolerated for 10pt FontX, that
means a dot diameter of .73 is your
minimum at 10 pt. Size changes
are proportional, so at 5 pt FontX,
the maximum dot diameter is .5 x
.73 or .37 pt. If printing at 133 dpi,
this means the threshold for screen
darkness is a little lighter than
40%.
This technique is in need of
more rigorous testing with a larger
sample of fonts and font styles.
In particular, while this series of
measurements works fairly well
for fonts with nearly even line
widths throughout, the numbers
fall apart for serif fonts, where
line weights vary dramatically.
Times Roman, for example, varies
at 10 pt from about 1 pt down to
about .35 pt. Italic versions (which
are conventionally used as hydrologic
labels, often over a tint of the
same blue ink the type is printed
in) are even more variable. A fontby-font
analysis would be necessary,
and perhaps the technique
outlined above will allow us to
make better judgements for specific
fonts.
 Number 47, Winter 2004 cartographic perspectives 59
Figure 5 Figure 6. (see page 84 for color version)
6. Light Lines
The same basic principles work
for light lines. On Hedberg Maps’
campus and community map
series, very light lines are used to
outline buildings. Over the years,
Hedberg Maps has used lines both
in a CMY color combination and
in a black tint. In both cases, lines
of hairline (.25 pt) and .5 pt weight
have also been tried. As Figure 5
shows, which color model is used
makes little difference. In both
cases, .5 pt is necessary.
In this case, it is the space between
dots that is important. In a
133 dpi screen, the dots are a little
over .5 points from center to center.
A similar effect could have been
achieved with a proportionally
narrower line if the screen frequency
were higher.
7. Screened Type
Figure 6 illustrates an example of
screening type. While it was noted
earlier that screened type does
not hold up well, at some point
the shapes become large enough
to hold up even when viewed
through a screen. As in the above
section on lines, the key seems to
be the ratio of the line weight to
the distance between dot rows.
Extensive testing has not been
conducted on this, but the type
appears legible when the letter
type weight is about 2 to 2.5 times
the distance between rows (i.e. the
inverse of the screen frequency).
The above applies to open
screens. For closed screens, the ratio
can get much closer, down to 1
to 1.5 times the dot-to-dot distance.
In these cases, the goal is that
nowhere will a “hole” or inverse
dot cut across a piece of type so as
to distort the type. As in screening
behind type, both open and closed
screen tints of type itself are much
easier in simple sans serif fonts
than in serif fonts.
Conclusion: Think In Inks
In the digital age, comparatively
little time is spent thinking about
separations and how they will
work together when compared to
the era in which each separation
was assembled manually from
a variety of elements. The automatic
nature of separation makes
life easier, but it also takes away
some awareness of how using the
four (or more) separate ink colors
can expand our design options.
Thinking in terms of inks is still
important.
For better or for worse, though,
this technique may be moot in a
few years time. Stochastic printing
has its own design challenges
(screens of light graduated tints,
for example, look quite different),
but it does make type composed of
almost any ink combination a viable
option. As it appears stochastic
is finally working its way into the
mainstream, color printing will in
the near future have resolved most
of what is discussed in this article,
opening up yet further design options
to cartographers.
Acknowledgements:
This paper was made possible by
Hedberg Maps, which provided
me the time to prepare it. Many of
the ideas in it were developed in
co-operation with my colleagues
there, especially Don Marietta.
Thanks also to my father, Marston
Case, for working through the
math with me.
References
Kennelly, P. and Kimerling, J.A.,
2003. Analytical Hillshading with
Luminosity from Aspect. Paper presented
at the Annual Meeting of
the North American Cartographic
Information Society, Jacksonville,
Florida.
 60 cartographic perspectives Number 47, Winter 2004
reviews
The Man Who Flattened the
Earth: Maupertuis and the
Sciences in the Enlightenment
By Mary Terrall
Chicago: University of Chicago
Press, 2003. 408 p., 3 halftone illustrations,
23 line drawings, bibliography,
index. ISBN 0-226-79360-5,
hard-cover. $39.00
Reviewed by Judith A. Tyner, Ph.D.
California State University,
Long Beach
Pierre-Louis Moreau de Maupertuis
(1698-1759) is not a household
name. He is not as familiar to
most as are Newton, Voltaire, Cassini,
or Celsius, other seventeenth
and eighteenth century names in
science and literature. Maupertuis
made no great discoveries and
many of his theories were contested
at great length and frequently
acrimoniously.
Maupertuis was a polymath
who studied and wrote on mathematics,
geodesy, astronomy,
biology, and metaphysics. He was
elected to both the French Academy
of Science and the Académie
français (literary society), a rare
honor for a scientist. He was
asked by Frederick the Great to
head the reformed Berlin Academy
of Science and Belles Lettres.
A high point of his career was his
expedition to Lapland to prove
that the earth was flattened at the
poles rather than elongated.
The Man Who Flattened the Earth:
Maupertuis and the Sciences in the
Enlightenment is an intellectual
biography of the man and what
it meant to be a man of science in
the Enlightenment, a time when
the way to such a career was not
clearly defined. The emphasis of
the book is on the subtitle; only
two chapters deal with the “flattening
of the earth”—one on the
expedition to Lapland and one on
the polemics that followed.
The book consists of eleven
chapters in roughly chronological
order that trace Maupertuis’s life
and thinking from his birth in St.
Malo to his death in 1759 in Switzerland.
Each chapter deals with
one or more of Maupertuis’s often
controversial papers or books.
The expedition to Lapland is described
in detail, but the following
chapter, “Polemical Aftermath of
the Expedition,” is the more entertaining.
Here the author details
the acrimonious writings of Cassini
and others who denounced
Maupertuis’s work as faulty
at best, and definitely shoddy
because a certain procedure was
not followed, and the rejoinders
by Maupertuis and Celsius that
pointed out that the new instruments
they used did not require
the procedure. It makes some
modern debates among scientists
seem cordial.
Another of Maupertuis’s controversial
works, Vénus physique,
is discussed in “Toward a Science
of Living Things.” This chapter
describes Maupertuis’s forays into
biology and natural history. Maupertuis
had, throughout his life,
performed numerous experiments
on animals from tiny water creatures
to dogs. He was interested
in reproduction and the formation
of the embryo—in the eighteenth
century it was not known how the
sperm and egg connected. Vénus
physique was written as a popular
book and directed toward a specific
unnamed woman explaining
how animals and humans reproduced.
As usual, his theories were
controversial as was the nature of
the book. Because it was written
to a woman, erotic in itself, and in
somewhat the style of contemporary
pornography, it created quite
a stir.
Vénus physique was only one of
Maupertuis’s “popular” works.
Especially since he wanted to
be elected to the select Acadé-
mie français, Maupertuis wrote
a number of works designed to
explain various aspects of science
to the educated public and
to promote himself as a man of
letters; one of these was Elements
of Geography.
The Prussian king, Frederick
the Great, when still crown prince,
desired to reconstitute the inactive
Berlin Academy of Sciences
and Belles-Lettres. His intent
was to establish an academy that
would rival those of England and
France. As early as 1738 Voltaire
approached Maupertuis to be the
head of the new academy. The
Berlin Academy was re-established
in 1744 and Maupertuis
assumed the position of President
in 1745. For this he had to resign
from the French Academy and
relocate to Berlin where he remained
until two years before his
death. Many of his writings at this
period were not only to advance
his own reputation, but also that
of the academy and the king.
Along the way, we are given
insights into science and society of
the time—the discussions in cafes
and salons, the interactions with
king and court, and the rivalries
and alliances between scientists at
the time. Discussions in cafes and
salons were important forums for
scientists and writers then. These
were not casual chats over coffee
as we might have now, but quite
often formal presentations. Wellknown
salons were presided over
by wealthy, educated women who
invited prominent scientists and
authors to participate. Maupertuis
was known in this milieu for his
social skills, witty stories, and
anecdotes.
Maupertuis, the man, comes
across as ambitious, somewhat
arrogant, and at times annoying,
although contemporaries found
him charming and sociable. He
was relentlessly self-promoting.
Today, we would say that he set 
 Number 47, Winter 2004 cartographic perspectives 61
goals and knew what it took to
reach them. His great desire was
to build a reputation “among
the social and intellectual elite,
including technically adept men
of science” (p. 8). His advice to
a colleague was to publish small
works often to keep one’s name
in the public eye. This isn’t bad
advice for junior faculty today.
This is a scholarly book, not designed
for the casual reader. There
are copius footnotes on every
page, usually over 100 per chapter
(one reaches 192). There are 22
pages of bibliography and a 16-
page index. The author, Mary Terrall,
clearly is well-versed not only
in her subject, Maupertuis, but
also in the time and the science of
the time. She is totally at home in
the period and knowledgeable of
the science, scientists, and scientific
disputes. The book is thoroughly
researched. Maupertuis’s
works were in French as were the
manuscript sources (correspondence
and some of Maupertuis’s
unpublished manuscripts). Professor
Terrall did all of the translations
herself rather than rely on
an outside translator so these are
not filtered through another’s
views. Often the original French is
provided in the footnotes.
The author assumes familiarity
with the period of the Enlightenment
and the people and places.
A reader who is not so conversant
with the time, may find
him/herself wishing for a cast
of characters at the front of the
book as in 1930s mystery novels.
Professor Terrell’s familiarity with
the period has led to some odd
omissions; latitude and longitude
are carefully explained to the
reader, but some terms unfamiliar
to a modern reader, e.g. fluxions,
are not. This is not a fast read or
a fast-paced story focusing on one
event or theory. Seeing the title,
one might assume that the book
is concerned primarily with the
story of the Lapland expedition,
a book along the lines of Dava
Sobel’s highly popular Longitude.
Anyone expecting that will be
disappointed, but one who is
looking for a thorough, scholarly
treatment of science and society
in the eighteenth century will be
pleased.
Cataloging Sheet Maps, the
Basics
By Paige G. Andrew
New York: The Haworth
Information Press, 2003
ISBN 0-7890-1482-3, hardcover;
0-7890-1483-1, paperback
$24.95, xv, 240 pp., tables,
illustrations, appendix,
bibliography, index.
Reviewed by Christopher H. Mixon
Map cataloger/curator
Auburn University Libraries
Auburn University
Auburn, AL 36849
If I have learned anything from
my experience as an Army Reserve
officer, it is this: You do not
have to know everything—just
where to find the answers. Now,
I am not by any means an experienced
cataloger. I have no formal
training to speak of. However, I
have been learning by doing in
the company of some very fine
experts for over three years now.
Some of my teachers, in addition
to the author, are acknowledged
in this book. I also owe a great
deal to bosses and coworkers who
show me the ropes every day. I realize
that this review is about the
book and not about me, but I feel
it is important to point out a bit
of my background because I think
it will help to emphasize how
vital this book is to those learning
to catalog maps. In the forward,
Alice C. Hudson points out that
increasingly map catalogers and
curators are not map specialists,
that the job of map cataloging has
been placed upon many who are
unfamiliar with the intricacies of
maps and map making. While this
is certainly true, and a key audience
for this book, my situation is
quite different. Having received
a geology degree and gone on to
study cartography in graduate
school, I have always dealt with
maps. Even as a child I was fascinated
by maps and became the
family navigator while on vacation.
As a map cataloger/curator,
I do not know which is more difficult:
having a good understanding
of maps but no cataloging experience
or vice versa. For the past
three years, I have made use of
the various sources on cataloging:
Anglo-American Cataloging Rules,
Second Ed., Cartographic Materials,
MARC 21 Concise Format for
Bibliographic Data, to name a few.
These essential tools have been
helpful yet often confusing. Cataloging
Sheet Maps, the Basics brings
the myriad of cataloging resources
into focus and points directly to
the particular manuals and rules
that pertain to specific tasks in
properly describing a map.
Cataloging Sheet Maps, the Basics
consists of five sections, each with
clear illustrations and, where
necessary, multiple examples of
various cataloging situations. The
first section, “In the Beginning,”
consists of a bit of background
into why basic map cataloging is
so important as more and more
libraries are making the decision
to bring their maps up to the level
of the rest of their collections
where cataloging is concerned. It
asks, “What is a map?” And “Why
bother to catalog maps?” These
are two very basic questions but
certainly worth asking. A cataloger
who is new to maps might do
better knowing what is considered
a map and may just be surprised
by the answer. Knowing and
understanding the parts that go
into a map is the key to describing
them. As for the second question,
I believe that anyone would agree
that anything worth having in a 
 62 cartographic perspectives Number 47, Winter 2004
library collection is worth describing
for their patrons. In the
second chapter of the first section,
the author jumps right into the
nuts and bolts of map cataloging.
He begins with some advice on
methodology and quickly points
out that each cataloger has to develop
his or her own style. Keep
in mind, however, that his advice
comes from many years of experience
and dedication to the craft,
which has helped shape some
of the rules we use currently.
The third chapter describes the
various publications that a map
cataloger needs to have at his/her
disposal in order to adequately
describe cartographic materials.
This list of resources is quite
extensive but is broken down
into basic, essential, and helpful
tools. Some are strictly electronic
sources, some strictly paper, and
some are both. The basic tools are
those that should be found in any
cataloger’s arsenal whether they
deal with maps or not, such as
Anglo-American Cataloging Rules,
Second, Ed. (AACR2R). The essential
tools are those that every
map cataloger should have at
hand such as Cartographic Materials:
A Manual of Interpretation for
AACR2R, and a measuring device
with centimeters on it. The helpful
tools include articles published on
map cataloging, helpful websites
such as Western Association of
Map Librarians “Map Librarians
Toolbox” and many others. Section
One finishes with the chief
source of information (the map
and/or its container) and prescribed
sources of information as
outlined in AACR2R that one can
use to derive the content that will
go into the various parts of the
bibliographic record.
Section two, “Coded Fields,”
describes the fixed fields and the
variable fields in the bibliographic
record, describing first those fixed
fields that are specific to cartographic
works and then those
fixed fields that are general. The
variable fields are treated in the
same manner but in much greater
detail—naturally since this includes
the 007 or physical description
field, which is mandatory and
must relate consistently with the
physical description or 300 field
and any notes that relate to physical
description. If a particular field
is repeatable and/or mandatory
and under what conditions is also
spelled out here.
Section Three, “Description of
the Map,” is by far the largest and
with obvious reason. After all this
is what it’s all about—describing
the map. It begins with a chapter
on Main Entry and Statement of
Responsibility. Main entry under
personal author versus corporate
body and the issues one encounters
when dealing with maps can
be sticky for a cataloger accustomed
to working with monograph
titles. This section offers
explanations of the rules involved
and lists terms one might find
on the map to aid in making
informed decisions. The next
chapter under “Description of the
Map” concerns entry of a title in
the record. This may seem like an
easy task and often is; however,
as the author points out, there
are many situations where the
title is not immediately obvious.
Many maps will often have more
than one title which may pertain
to the same main map or two
titles for two separate but equally
important maps on one sheet.
Sometimes maps bear no title
at all. There is information here
for dealing with just about every
situation regarding titles. Next is a
chapter on edition, which describes
the importance of a map’s
edition in a description due to a
map’s graphic nature and how
small changes in a map from one
edition to the next could have a
marked impact. The next chapter
concerns the mathematical data
area, which makes me thankful
that I have a cartographic background.
To many catalogers starting
out with maps, this can be like
learning a new language. This will
become easier with exposure and
experience, as the author points
out. Much of this chapter is a bit
of a lesson in basic map reading
and discusses concepts related to
map scale, projection, and map
coordinates. Map scale and how it
is depicted on a map, concepts of
large scale versus small scale, and
how to correctly enter this information
in the record are discussed
at length. I have seen professors
of Geography become confused
about scale so you can imagine
how this subject can scramble a
new map cataloger’s grey matter.
Using clear language and illustration,
the author does an excellent
job of describing scale and how
it is to be depicted in the map
record. Projection, while a potentially
difficult concept to grasp,
is not discussed in great detail
primarily because it is not necessary
to fully understand the idea
in order to place it in the record.
Recording the map’s coordinates
is not as simple, however, and
although optional, the author
urges its use whenever possible as
this allows for mathematical access
to maps contained within the
stated coordinates. This chapter
carefully outlines the process of
recording coordinates and even
extrapolating coordinates where
the mapped area extends outside
of the printed coordinates. A
boxed section in this chapter gives
a quick look at rules of thumb and
provides situational examples for
recording coordinates. Publication
information is discussed in
the next chapter. This is information
that is not always evident on
many maps. Even dates can be
nonexistent or encrypted on some
road maps. Publication date versus
date of situation is discussed
here also. Next is an extensive
chapter covering the physical
description area or 300 field. For
this area in the record, the author
describes various ways that maps 
 Number 47, Winter 2004 cartographic perspectives 63
can be put together such as map
sets, map series, a map on multiple
sheets, and how they should
be dealt with. In this section the
descriptions start with the simplest
situation and end with the
most complex situation for the
physical description. The chapter
then continues with how to record
a color versus monochromatic
map and how to deal with two
sided maps and the application
of recto and verso. Finally the
chapter discusses measuring the
map and how, where, and when it
is appropriate to measure the map
and/or its container. This is where
that tape measure with centimeters
is put to good use. Section
three concludes with a chapter
devoted to including notes in the
record. This chapter contains a list
of essential notes and additional
information notes pertaining to
cartographic materials. There are
examples listed categorically dealing
with map description at the
end of the chapter.
Section four, “Other Access
Points,” deals with other items in
the record to assist the searcher
in finding the right map. Beginning
with classification using the
Library of Congress (LC) G-schedule,
the author breaks down a
typical LC call number into its
component parts and discusses
each thoroughly. LC is the only
classification discussed in the
book since it is the most widely
used classification system for
maps. Then the section moves on
to a chapter on subject analysis for
maps. This chapter refers heavily
to specific resources related to
subject analysis. This provides a
close look at geographic subject
headings and guidelines for indirect
and direct geographic subdivision.
The section ends with
a chapter dealing with how and
when to place added entries in the
record and its justification.
The fifth and final section,
“Historical Sheet Maps and
Special Cases,” begins with the
fact that individuals rather than
corporations or agencies are more
often given main entry and that
it is important to include everyone
involved in statements of
responsibility. Secondly, titles can
be a special problem in historical
maps and there are references to
the rules on how to handle these.
Scale conversion from historical
units of measurement to today’s
units based on miles is listed
under mathematical data as is
projection and conversion of coordinates.
Like scale, coordinates
require conversion to today’s
system and this is explained
here. The chapter discusses how
to provide publisher, place and
date of publication and how to
deal with placenames that have
changed since the map was made.
There is some discussion about
the rules that govern physical description
that reflect how the map
was produced. The chapter lists
unique notes that are essential in
describing historical maps such as
the description of a watermark or
information regarding donor or
previous owner. Finally, subject
subdivision and particular subject
practices are covered, such as the
use of current geographic names
over historic names in the subject
entry. The final chapter relates to
special formats and situations,
starting with specially mounted
wall maps and how this affects
physical description. Next, the author
covers cataloging map series
by touching on each element of
the bibliographic record, including
relevant coded fields, title,
mathematical data, physical description
area, etc. The next item
discussed covers the handling of
indexes, text, and other supplementary
items relevant to the map
itself. Then finally a reference to
resources on how to handle various
map reproductions completes
the final chapter.
The appendix consists of practical
exercises, with answers, where
the reader is asked to supply appropriate
entries in 034 field (projection),
the 052 field (geographic
area code), and 300 field (physical
description).
In my opinion, Cataloging Sheet
Maps, the Basics, accomplishes
the long overdue task of bringing
together everything one
needs to know to create good
map records. It does not supply
all of the answers, but it provides
excellent examples, and refers you
directly to the sources of authority
on every aspect of a good map
record. I not only recommend this
book but I urge anyone cataloging
maps to include it in their arsenal,
whether you know map cataloging
or are new to it all. With this
tool in hand, the various resources
will be so much easier to navigate
when answers are needed.
Obviously, if you are an experienced
cataloger new to maps, it
is the cartographic stuff that is
most troubling and this book will
definitely ease your mind. For me,
however, Cataloging Sheet Maps,
the Basics will become my portal
into the vast cataloging resources
available for cataloging sheet
maps. 
 64 cartographic perspectives Number 47, Winter 2004 
 Number 47, Winter 2004 cartographic perspectives 65
Cartography 2003
Figure 3. A public domain reference map of Azerbaijan. Source:
CIA Factbook, 2003.
Figure 4. A property map on the web. Source: Cabarrus County,
2001.
Figure 6. A map from Mapping Census 2000. Source: Brewer
and Suchan, 2001.
Figure 5. Excerpts from “A Tapestry of Time and Terrain.” Image
has been rearranged for this illustration. Source: Vigil et al., 2003.
 66 cartographic perspectives Number 47, Winter 2004
Figure 8. The layout of a Colorbrewer page. A diverging 5-class
scheme is illustrated here. Source: Brewer, 2002. Used with
permission.
Figure 9. The 1910 population image in Jill Hallden Harsha’s U.S.
population animation. Used with permission.
Figure 10. An historical map of Yosemite Valley, the current-day
digital elevation model (DEM) of the same area, and the historical
map draped over the DEM. The image in the lower right is a
scene as one flies over the area. Source: Rumsey, 2003. Used with
permission.
Figure 13. A screen capture of a page in the interactive Atlas of
Oregon. Used with permission. Source: Atlas of Oregon CDROM,
Copyright 2002, University of Oregon Press.
Figure 11. An image created from Shuttle Radar Topography Mission
data. Source: JPL, 2003.
 Number 47, Winter 2004 cartographic perspectives 67
Mapping September 11, 2001: Cartographic Narrative in the Print Media
Figure 7. “Osama’s World”, Time, 24 September 2001. ©TIME, Inc. Reprinted by permission.
 68 cartographic perspectives Number 47, Winter 2004
Figure 9. “The Options for Battle”, Newsweek, 15 October2001. ©2001 Newsweek, Inc. All rights reserved. Reprinted by permission.
 Number 47, Winter 2004 cartographic perspectives 69
Hal Shelton Revisted: Designing and Producing Natural-Color Maps with Satellite Land Cover
Data
Figure 1. A portion of Hal Shelton's 1:5,000,000-scale New Europe map painted ca. 1968. The original
measures 107 x 137 centimeters. Drainages and water bodies are photomechanical additions to the
original art. Courtesy of Rand McNally & Company.
Figure 2. Shelton's standardized palette of natural colors captured the character of disparate geographic regions worldwide. Courtesy of Rand McNally &
Company.
 70 cartographic perspectives Number 47, Winter 2004
Figure 3. (left) Excerpt of a natural-color map painted by Hal Shelton ca. 1968. (right) NASA MODIS
satellite image taken in 2003. Map on left courtesy of Rand McNally & Company.
Figure 4. (left) A shaded relief map of southwestern United States combined with natural colors.
(right) The same map with blended hypsometric tints. Although hypsometric tints are attractive and
show topography clearly, they can mislead readers about the character of the land. Forests cover the
Yellowstone region and Yuma, Arizona, is an extreme desert environment.
 Number 47, Winter 2004 cartographic perspectives 71
Figure 5. (left) Tibor Tóth's color formulas. (right) His colors applied to a map. Courtesy of National Geographic.
Figure 6. NASA's "Blue Marble" photograph
shows Earth from a distance of nearly 48,000
kilometers (30,000 miles). Dominated by the
Sahara and Kalahari deserts, Africa is usually
the most cloud-free continent. Antartica is also
visible in this image for the first time (NASA,
2002).
 72 cartographic perspectives Number 47, Winter 2004
Figure 8. A natural-color Landsat image of the Grand Canyon made from bands 2, 4, and 7. Even the
handsomest satellite images contain graphical elements inconsistent with cartographic design goals.
Courtesy of the USGS.
Figure 7. (left) "The Living Earth." (right) NASA's new "Blue Marble."
 Number 47, Winter 2004 cartographic perspectives 73
Figure 10. (left) The NLCD classification with USGS assigned colors. (right) The derivative color palette used for natural-color mapping.
Figure 9. NCLD mosaic of the 48-contiguous states, using the USGS suggested color scheme.
 74 cartographic perspectives Number 47, Winter 2004
Figure 11. Color groupings in the palette.
Figure 12. California and the southwestern US depicted with colorized NLCD and shaded relief.
 Number 47, Winter 2004 cartographic perspectives 75
Figure 13. Using the Color Table in Adobe Photoshop with NLCD in indexed color mode to convert USGS colors (left) to natural colors (right).
Figure 14. Blended lands cover categories in MODIS VCF. The combined values for any sampled pixel
on the map are 100 percent.
 76 cartographic perspectives Number 47, Winter 2004
Figure 15. (left) MODIS VCF in Photoshop presented as uniform colors. (right) With environmental color adjustments applied to the herbaceous layer.
Figure 16. (left) The final map primarily based on MODIS VCF data. (right) The top five Photoshop layers contain supplemental data added to the MODIS
VCF base.
 Number 47, Winter 2004 cartographic perspectives 77
Figure 17. Shaded relief merged with a natural-color base made from MODIS VCF data.
Figure 18. (1–4) Variants of the traditional legend. (5) A natural legend. Legend portrayal becomes less abstract and more cartographically realistic from left
to right.
 78 cartographic perspectives Number 47, Winter 2004
Figure 19. Using the Median filter to generalize urban land cover data.
 Number 47, Winter 2004 cartographic perspectives 79
Figure 20. Using the Actions palette in Photoshop to spread shoreline pixels outward.
 80 cartographic perspectives Number 47, Winter 2004
Figure 21. Removing drainages from NLCD with the Dust & Scratches filter.
Figure 22. "Canyon Lands" by Hal Shelton. Millard Canyon, Utah, dominates the center of the scene and the snowcapped LaSal Mountains are faintly visible
on the right horizon. The vertical triptych joints do not appear because of digital compositing. Courtesy of Library of Congress.
 Number 47, Winter 2004 cartographic perspectives 81
Small Type, Screens and Color in a PostScript Offset Printing Environment
Figure 1.
 82 cartographic perspectives Number 47, Winter 2004
Figure 1a.
 Number 47, Winter 2004 cartographic perspectives 83
Figure 3a.
Figure 2.
 84 cartographic perspectives Number 47, Winter 2004
Figure 3b.
Figure 6."""
  const tests = ["", "abc", "xyz", "man", "leasure.", "sure.", "easure.",
                 "asure.", longText, longText, longText, longText, longText, longText, longText, longText, longText, longText, longText, longText, longText, longText, longText, longText, longText, longText, longText, longText]
  for t in items(tests):
    assert decode(encode(t)) == t
