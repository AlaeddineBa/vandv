#!/bin/bash

if ! [ -f $1 ]
then
  echo "file was not found"
  exit 2
fi

#delete nimcache
rm -rf nimcache/

# get filename
filename=$(basename $1)

#get filename without extension
filename="${filename%.*}"


# test if file already compiled 
if [ -f "$(pwd)/nimcache_C_$filename/nimcache/$filename.c" ]
then 
	echo "file already compiled to c : please delete nimcache_C_$filename folder and compile again "
else 
	#compile nim to c

	echo "####################################### Compiling to C #######################################"
	echo "--------------------------- compilation time : -----------------------------------"
	time (nim c -r $1);

	echo "compilation succeeded to c"
	echo "--------------------------- Memory (valgrind) -----------------------------------"
	#valgrind --leak-check=yes ./$filename > result.txt 2>&1 
	cat result.txt|grep total
	rm result.txt

	echo "---------------------------execution time -----------------------------------"

	#time ./$filename

	# save generated files into "nimcache_C_$filename"
	mkdir "nimcache_C_$filename"
	cp $filename "nimcache_C_$filename"
	cp -R nimcache/ "nimcache_C_$filename"
	rm -R nimcache
	rm $filename
fi


if [ -f "$(pwd)/nimcache_JS_$filename/nimcache/$filename.js" ]
then 
	echo "file already compiled to js : please delete nimcache_JS_$filename folder and compile again "
else 
	#compile to js
	echo "####################################### Compiling to js #######################################"
	echo "--------------------------- compilation time : -----------------------------------"
	time (nim js -d:nodejs -r $1);

	echo "--------------------------- Memory (valgrind) -----------------------------------"
	#valgrind --leak-check=yes node "nimcache/$filename.js"  > result.txt 2>&1 
	cat result.txt|grep total
	rm result.txt
	
	echo "---------------------------execution time -----------------------------------"
	#time  node "nimcache/$filename.js"
	
	echo "compilation succeeded to js"

	# save generated files into "nimcache_JS_$filename"
	mkdir "nimcache_JS_$filename"
	cp -R nimcache/ "nimcache_JS_$filename"
	rm -R nimcache
fi




if [ -f "$(pwd)/nimcache_CPP_$filename/nimcache/$filename.cpp" ]
then 
	echo "file already compiled to cpp : please delete nimcache_CPP_$filename folder and compile again"
else 
	#compile to cpp
	echo "####################################### Compiling to C++ #######################################"
	echo "--------------------------- compilation time : -----------------------------------"
	time (nim cpp -r $1);
	echo "compilation succeeded to cpp"
	echo "--------------------------- Memory usage (valgrind) -----------------------------------"
	#valgrind --leak-check=yes ./$filename > result.txt 2>&1 
	cat result.txt|grep total
	rm result.txt
	

	echo "---------------------------execution time -----------------------------------"

	#time ./$filename
	# save generated files into "nimcache_CPP_$filename"
	mkdir "nimcache_CPP_$filename"
	cp $filename "nimcache_CPP_$filename"
	cp -R nimcache/ "nimcache_CPP_$filename"
	rm -R nimcache
	rm $filename
fi


exit

