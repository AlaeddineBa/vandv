## **Non-functional Testing of Nim code generator** ##

Nim is a compiled programming language that is able to generate code to different target platforms C, C++ or Objective-C and also JavaScript.

**Nim Installation**

The following commands can be used to download  the latest development version of Nim :

```
#!unix

$ git clone https://github.com/Araq/Nim 
$ cd Nim 
$ git clone --depth 1 https://github.com/nim-lang/csources 
$ cd csources && sh build.sh 
$ cd .. 
$ bin/nim c koch 
$ ./koch boot -d:release
```


After you have finished the installation, you should add the nim binary to your path.

```
#!unix

$ echo 'export PATH=$PATH:$your_install_dir/bin' >> ~/.profile
$ source ~/.profile
$ nim

```

After finishing the installation , you can execute the script [./compile_nim_file.sh filename.nim](https://bitbucket.org/AlaeddineBa/vandv/src/d6f79fad054cad195341bb582f019899be6bfb12/compile_nim_file.sh?at=master&fileviewer=file-view-default).